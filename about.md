---
layout: page
title: ME
permalink: /about/
---

![]({{site.baseurl}}/images/HDYDY.jpg)

My name is Julia and I'm from Argentina. As most of my fellow countrymen, I'm a descendant from Italians and Spanish immigrants, so I have the features and characteristics of both cultures.

Most of the time I'm an architect, but also I have many interest in some other fields, as urbanism and sociology, graphic design, scenography and interior design; visual arts in general. I introduce myself as a curious mind, as a person that has a personal idea or vision, but also aims for a team to work with. I believe in the interaction and in multidisciplinary projects. I'm not in agreement with the idea of an only and unique author; I support the idea that the final work is made through many stages and by many hands that cooperate and make a project more complex. I love to work in the line of complexity. The human being is a complex individual and my goal is to get near and embrace that whole unity.

I like to communicate and I know that I'm good at it. I certainly believe that each person has dreams and also abilities; I prefer to work with both variables: challenging your limits, but also using your inner attributes and trying to get the best of them. Balance is something that we always should seek.

Challenge is what attracts me. When something or someone is challenging me, this wins my attention and disposition. I like to cultivate myself as much as I can; also, to move, to search, to find and to lose again: always at the starting point, always ambitious for more.
I would like to take my professional career in such a way, trying to always get the best of any project; asking once and again questions to expand the solutions. In that sense parametric architecture fits me perfectly. Considering as an architecture by variables, in constant movement and possibilities, having the choice of testing and modifying these parameters. Not giving a final project, instead a project with to grow. Also a project that works for, in and with the spatial surroundings or the social activities involved. And in the other hand, with a non-individual author at all, but a multidisciplinary production.

That’s why these particular Master caught my attention. Because of its diversity, its questioning attitude, the constant search to reach for more of what we see and experience at the present time. Furthermore, it is a challenge for me to make decisions, to stop designing and to materialize at once the possible solutions, the possible devices/interventions or social experiences; and in that way MDEF puts me in constant scrutiny.

At this time I will take the post and will do my best to take advantage of it!   

LET´S START ENJOYING!
