---
title: 06. Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

#DEFINE THE RULES

An **Intelligent Object** will be programmed considering a network of different layers and actions which correlates each other, working by stages and other times, simultaneously. We will start by understanding which is our **GOAL**, which are the **TASKs** we will carry out to achieve such goal, which are the **INPUTS** that we will collect and the **OUTPUTS** we will offer at the end, and which is the **CONTEXT of operation** for our project.
The GOAL has to be something specific that we want to OUTPUT, from where we can propose a list of TASKs to operate by steps or at the same time and level. Our INPUTS will come from the environment around our project, they will become the information, and the DATA that will help us to notify the Intelligent Agent and that will be in relation with the CONTEXT of operation.

The **5W + 1H** is a method of questions whose answers are considered basic in information gathering. We can correlate each **W** with our actions described before to help us to organize the information and to look upon everything that matters. It’s important to realize that the reliability and sincerity of our Object depends on the material that we will provide to it. The **H** links to **HOW** we are going to operate and what actions will be carried out.

When we want to specify our GOAL and the **WHAT** in our project, it is important to make a difference between the *REAL and present ENVIROMENT*, what we count with, and the future that we want to reach. What is the actual situation of our field of interest or with whom we can associate for the same purpose; it’s a way of engaging with reality. In this particular stripe the goal will be a new *re-presentation*, innovating in *data visualizations* and new visual displays.

To start to trace the TASKs, we should take into account the procedure and the time to act. We should design **WHEN** we are going to collect the DATA that will be important and related to our project, the *THICK DATA* and the *methodology* that we need to apply in order to achieve our objective. Technology as *Sensors* and facilities for merging and collecting data will be useful for these actions.

As regards INPUTS, we cannot unlink ourselves from the CONTEXT of operation, only because we are *analyzing and learning* about this existing background. We will have to ask ourselves **WHO** and **WHERE** the sources for training our Object are. In this essential task we will also consider the *ACTUAL ENVIROMENT*, the bases we count on as well as the upcoming users for our project. The methodology of *Machine Learning* should be applied to cluster, filter and classify the information gathered; for later *recording* the right content, constructing the right labels and predicting the necessary adjustments.

Now we are reaching the point of the network where we have to ask ourselves **WHY** we are designing our virtual machine. The answer is easy: the aim is to understand intelligence and to reconstruct it artificially, using smart technologies to help society, and so, we have to make *decisions, take actions and reach new policies* based on everything we have identified before.

We want to know what understanding and happiness mean, what to be human truly means. To extend that power to the machine will allow us to comprehend the real, sincere and objective human being. It will show us the bias, the mechanic and sensible decisions that we take. We will feed devices on the purpose of exploring social behavior.
*Humans feed the machine, humans control the machine’s desires, and humans consume the products of the machine. And right now, humans still make the decision to turn the machine on or off. (Not-So-Autonomous, Very Human Decisions in Machine Learning: Questions when Designing for ML / Henriette Cramer, Jennifer Thom / 2017)*


*THE GLITCH CHAMBER*

[PROTO PIXEL](https://vimeo.com/98075534 "PROTO PIXEL")

![]({{site.baseurl}}/Canvas.png)
