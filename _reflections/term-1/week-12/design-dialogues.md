---
title: 12. Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/MAP FINAL.png)

# - be CREATIVE IN THE LOGIC -

My project is about language and how the different ways of communicating can give us information.
We have several ways to communicate and as a social being we decide which language suits us better. But sometimes we find out that the spoken or written language, for example the different idioms, do not complete our inner expressions. I would like to focalize in the meaningful patterns behind the way we exchange information; to codify our behavioral patterns, the one implicated in our gestures and the ways in which we organize it.
I believe that there is a secondary language in this aspect. A language that can help us to reduce the gap between what we communicate, what we demonstrate and what we design. This information, that nowadays is still unmappable, should be used in our spatial and management designs, to approach Happy and participatory Cities. My challenge is to embrace all our complexity, our dynamics and associative processes, trying to design a new and inclusive language.

My workspace is the gap between human behavior and the design that we as architects, engineers and designers deliver. The data that we collect and use for this matter is, in my way of thinking, limited. I believe that we are leaving behind other data that would be useful to stretch this gap and by the end, to construct a social environment. The thick data that we are not taking into account is out there, in the way we communicate, beyond our main language. We have some idioms that are more in touch with this thick data, as the Chinese ideograms or Arabic calligraphy that are structured with another logic, than the Latin alphabet.
Also, we can analyze the language of Nature and how this structure responds in a fractal and associative way, considering coexistence and passive growth.
![]({{site.baseurl}}/U2.png)

<https://www.chineasy.com>
#### *“Beautiful Brain: The drawings of Santiago Ramon y Cajal“ 2017*

By the end I found myself with a hypothesis that has to be proved by research and experimentation and in consequence, it has to be test in a prototype as a new way of communicating.
![]({{site.baseurl}}/U1.png)

On one hand I can work in the structure of language and try to relate it with a structure of a building. For example as PATTERN LANGUAGE*, where the language becomes extremely practical. This language formed by patterns is a new convention, derived from buildings and planning, so it can be used in a new process of construction. For example we have a poem that is understood not in a single sentence, one after another, but it is read and interpreted as a whole. The same happens with a building, where the patterns are rearranged and overlapped for providing different sensations and also interpretations. But in poetry we are using the same language, the same patterns as in the written idiom, the only thing that changes is its organization; on the other hand, buildings have to invent and discriminate their own patterns.

*… In short, no pattern is an isolated entity. Each pattern can exist in the world, only to the extent that is supported by other patterns: the larger patterns in which it is embedded, the patterns of the same size that surround it, and the smaller patterns which are embedded in it.
This is a fundamental view of the world. It says that when you build a thing you cannot merely build that thing in isolation, but must also repair the world around it, and within it, so that the larger world at that one place becomes more coherent, and more whole; and the thing which you make takes its place in the web of nature, as you make it.*  
#### ¨A Pattern Language¨ / Christopher Alexander 1977

So, we can have a building for functionality that will use some patterns and a particular way of organizing them, and we can have also a building for experience that will use, perhaps the same patterns or new ones, but which is organized in a different way. In the middle we can found a FUN PALACE from Cedric Price, which works the flexibility in the design, allowing to rearrange the space in an unpredictable mode, moving from functional to experience.
*… concept of an interactive, performative architecture, adaptable to the varying needs and desires of the individual.*
#### ¨The Fun Palace: Cedric Price’s experiment in architecture and technology¨ / Stanley Mathews 2005
![]({{site.baseurl}}/FUN PALACE.jpg)

This relates me to the inner structure of the brain, where we find different elements as patterns that work and relate with each other in an efficient configuration. We could go further and also study the neurolinguistics as the relation between language and the functioning of the brain.

As a conclusion for this proposal we can find out that implementing the same logic as we do in constructing a new idiom, we can use different elements, regroup them as patterns and start to write down and to document this new logic of language. It is important to deliver at the end a catalogue that anyone can manage and use it for his own purpose.
Digital language can be very useful for this approach as well, in the way that is a language that has a binary structure, and from there an exponential complexity. But all starts from two components and how these can be combined to complicate and enlarge the system.
