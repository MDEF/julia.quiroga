---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---
#THE FOURTH DIMENSION  
When we talk about ENGAGING NARRATIVES, it comes to my mind many ways of telling a story. Of course we have the typical structure of storytelling that starts with a presentation of the characters or project and the actual environment, a descriptive introduction of the story. Then we have the developing of the tale, where action takes part and things start happening; here also is where the story achieves its own meaning, the reason why it becomes a good and attractive narrative. At the end we are confronted with the outcome, the resolution and ending of the story, how and what happened or is going to happen and which is its acknowledgement. Each part has an inner structure and correlates with other parts in a sequential way.
This is the way we communicate a story and this is also the way we succeed in understanding our project. If we can disassemble this basic structure and try to operate in another organization, perhaps we’ll get to communicate in a natural and more consequent manner. Let me share some thoughts with you.

![]({{site.baseurl}}/ArtboardCopy.jpg)

I keep on thinking that we as human beings have a different kind of intelligence, in relation to the way we communicate our ideas. I understand that it’s important to arrange a common language or structure, so everyone can understand each other; but I think that in this way of simplifying and narrowing our manners, we are losing some information.
If only could we have a complex and global narrative, one that can involve our parallel thoughts, our connections, networks and the existing interrelations.

![]({{site.baseurl}}/Afghanistanstability.gif)

As regards language, for example, we have different configurations. First of all and before getting to the structure of languages, I would like to make clear that language (for me) is one of the ways of communicating that a society has chosen and agreed to own, it is not the only one. But it is the most widely used and, in my belief, the way you can get quite a bit of information about people. From the way we arrange our language, the priorities set up within its general structure, if we emphasize adjectives or nouns or invert tenses, if we have different conjugations for feminine, masculine and plurals, every particularity counts and I honestly think that we have to analyze and take all these features into account.   
It is not the same to write down in one sense or in another, or to write down some letters that, once assembled, show us some words, that also these words assembled give us a sentence and a comprehension of an idea. We have some languages that work in a sequential way and others in a gestalt (or immediate) way, if we can say so. Gestalt is a theory that proposes that the whole object or scene is more important than its individual parts. And the way we understand these meanings, how we build and decipher the phrases is not the same. The cognitive process that lies under each is totally different and implies a Cosmo vision of its own.
There are different connotations or even meanings for the same word, and if we contextualize them this word could mean one thing or another. It is not the same to have a dynamic language in a graphical manner or even in a structural one that keeps a constant movement and evolution.
All these considerations about language, should give us very useful information about the society and people. Which are their priorities, their ways to relate to one another, and the way to acknowledge information or to deliver it?

![]({{site.baseurl}}/chineselanguage.jpg)
![]({{site.baseurl}}/LANGUAGE.jpg)
![]({{site.baseurl}}/ABC.jpg)

So if we start by accepting this starting point regarding language, and consider the way we design it in a global and written way, the narrative that we can achieve from this basic structure, should correlate with it. As we speak and write, we communicate and we tell a story. In this lineal sequence, I believe that much information is lost. Let’s go beyond written language, let’s trace information in the other forms of communication.
Let’s take body language for instance. It has already been studied that the body behaves and demonstrates clues of our inner thoughts and feelings. Some of them we can control and some others are unconscious and spontaneous, even though we can’t perceive them, but we still desire.

![]({{site.baseurl}}/BAILAR.jpg)

*¨… He must become a connoisseur of the gradations between the automatic, the spontaneous, the evoked.¨ — Harold Rosenberg, 1952*

**Is it possible to map or to draw, or even to write down this DATA?**
I believe that it’s possible and I will bring an example to demonstrate this.

The SUFI CALLYGRAPHY is *“the most perfect, rich and ancient of the designs”*, because of the versatility, energy and symbolism of its letters. *“Each letter of the alphabet, as an abstract form, fulfills a specific meaning, and these letters, with their differences, are in their expression a source of inspiration.”*
*MadīḥaʿUmar (1909-2005), “Imágenes abstractas de las letras árabes” Museum Corcoran / Washington / 1949*
These characters that explore the plastic possibilities of figures are distinguished from those of other alphabets by *"continuity, rhythm and abundance of forms"*
In this context I would like to consider and compare this to the act of dancing, such as the art of lettering/movement and also the art of dance in a more literal manner, regarding this as an unlimited movement, which goes beyond formats, disciplines, spaces, time and even ideas. The action of dancing breaks boundaries and makes visible the unexpected territories, showing the infinite possibilities that creation has of capturing and transforming reality, inventing futures and projecting the most real desires.  
We will reach to a body writing that invents new alphabets and generates new vocabulary; new codes that are inscribed on unanticipated bodies in a disconcerting movement.
*“By releasing the sign of the linguistic code it confers absolute universality.” NjaMahdaoui (Tunisia, 1937)*

![]({{site.baseurl}}/sufiesespaña.jpg)
![]({{site.baseurl}}/sufiesbaile.jpg)
![]({{site.baseurl}}/sufiesletra.jpg)
![]({{site.baseurl}}/contemb.jpg)

*“We use a variety of different codes for communicating among ourselves because some codes are more convenient than others. For example, the code of the spoken word can't be stored on paper, so the code of the written word is used instead. Silently exchanging information across a distance in the dark isn't possible with speech or paper. Hence, Morse code is a convenient alternative. A code is useful if it serves a purpose that no other code can. “
Code: The Hidden Language of Computer Hardware and Software / Charles Petzold / 2009*

In other words, a code lets you communicate.

## VIDEO LET'S DANCE 
