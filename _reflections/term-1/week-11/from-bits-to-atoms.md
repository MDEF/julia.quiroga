---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---

# Scribble Sound – Let’s MAKE SOME MUSIC!

### Day #1: Ideation of music machine and its prototype
Together with Saira Raza, Silvia Ferrari, Gábor Mándoki, Oliver Juggins and Ryota Kamio, we formed a team to create a music machine. Our idea is to make a machine that makes different sounds by drawing black lines on a white surface.

![]({{site.baseurl}}/inspiration.jpg)
![]({{site.baseurl}}/DSC0828.jpeg)

### Day #2: Design development
We modeled the prototype in digital format, using Rhinoceros and worked on the coding part for the sensors and MIDI signals in Arduino.
First, we gathered and designed the materials and parts for our Scribble Sound machine. Our neighbor, BICICLOT (Carrer de Pere IV, 58, 08005 Barcelona) gave us used rubber from bicycles for the music belts and we cut them in half to fit in the size of the machine. Later we 3D printed spools so that the music belts move smooth. The fabrication took 4 hours for two pieces.

![]({{site.baseurl}}/iso.png)
![]({{site.baseurl}}/DSC0807.jpeg)
![]({{site.baseurl}}/DSC0821.jpeg)

### Day #3: Design process
We changed from the black rubber to a white fabric in the end because the rubber was too heavy for our set up and black on white delivered a better contrast for the sensor. Also we changed our original DC motor to a higher torque and lower speed model which could be powered by a 5V power supply. We also decided not to use a potentiometer to control the speed as we blow up a computer during the process!

![]({{site.baseurl}}/sensorSetU.png)
![]({{site.baseurl}}/DSC0845.jpeg)

### Day #4: Adjusting the prototype
On day 4 we finished producing the 6 remaining wheels. The printer we used lacked the support we would have needed to create the final dividing edges of the wheels, but the belts worked anyway. Totally, it took 18 hours for us to produce 8 wheels. In retrospective, since we didn't need the edges on the wheels, we could have used CNC milling to produce them. We also created cardboard support for the four sensors.

![]({{site.baseurl}}/DSC0829.jpeg)
![]({{site.baseurl}}/DSC0841.jpeg)

After finishing the production, we put the modeled parts of the prototype together to see how they worked. The structure was too weak to tolerate the movement of the motor, wheels and belts, so we decided to improve its sturdiness by adding wood bridges and modifying the structure hosting the motor.

### Day #5: Final assembling
The final assembling went very well. We had to adjust the sensor here and there but in the end everything fitted perfectly in the housing. We attached a battery as a power supply to the Raspberry Pi and attached a speaker to the headphone jack. Now the machine played drop sounds we created before whenever a black shape passed by.

## VIDEOhttps://youtu.be/BS5BNDIuOpk

#### All files and guide you can find here: GitLab.com/MDEF/ScribbleSound
