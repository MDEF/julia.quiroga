---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

#01. MDEF BOOTCAMP

First week of the Master and I have already started my personal and professional brain storming.
I find myself again at the beginning of everything, when I thought I had a clearer idea of certain guidelines.

**FLEXIBILITY** is the main word that comes to mind when I have to summarize this first week.
Flexibility for my own ideas and preconceptions, flexibility on my own projection for future works, flexibility on my way of doing and learning things.
It’s interesting to see how flexibility, something that I myself try to inspire in the people I work with or for, is an issue that sometimes is really difficult to experience in my own life. I feel quite structured and constrained when this matter presents itself to me.
Being at this instance, aware of this difficulty, helps me to see the bigger picture and to understand, from my own experience, that changes have various edges, steps and variables to consider. It isn’t just black or white, there’s not only one way to approach improvements. The path to get to a HAPPY CITY has several and mixed ways, and it would be a shame to exclude any of them.
Another lesson, pretty ironic, during this first week, is that the more I think I know or the nearer I feel to my true goal, the less I focus in only one outcome.
**Open mind**, **co-operation** -mainly with yourself-, flexibility and humility are the headlines for my personal boot camp.


The first time I introduced myself to the class and discovered my voice, I realized that some lines in my discourse weren’t completely joined. This makes me think about something I do not implement often: **TO LISTEN**. I believe that this action describes the main attitude, the main key to many solutions. If we, as social actors, could be able to listen, to observe, to try to recognize and to apply real and needed solutions, instead of being capricious and indifferent; I’m quite sure some realities would be different.
I encourage myself and my workgroup to listen, to really commit with the necessities this city or any city has. And this relates to each and every person and user of it. I am aware that it’s not possible to give each one of them a ready answer, and also that there are many considerations we need to have in mind when we suggest urban changes; but I insist that we should try to get to the foundation, to the real and main character that walks and experiment our environment day after day.
Return to the needs that we have as humans and social beings:
the need to be around other people, to socialized,
the need to walk freely, to be in contact with nature,
the need to feel part of somewhere, to feel inclusion and a sort of equality,
the need and desire to feel and experiment HAPPINESS.

In an optimistic, and frequent way of thinking, I challenge myself to achieve this knowledge and to put it in action in the pursuit of my work. I see myself as a maker and also as a main user of the urbanism improvements. I would like to be capable of playing both parts of this play, prioritizing my sensibility sometimes and my leadership other times, while collecting information that would be useful for their implementation and further performance.

As a conclusion, I will refer to another concept that jumps into mind and that I believe joins all the previous ones: **RECYCLING**.
**Recycling** as an approach to **restore** our natural behavior as contributors, and also the resources that we have for urbanism purposes.
I would like to deepen in this concept, to understand and learn the action of reinserting something back into the circle of everyday reality. Not something only tangible but also behavioral.

![]({{site.baseurl}}/week1.png)


##FOOTNOTES AND OTHER REFERENCES:
-	**HAPPY CITY: Transforming Our Lives Through Urban Design**, by Charles Montgomery, 2013, Vancouver, Canada
<https://thehappycity.com/the-book/>
-	**ÚS BARCELONA: ritual d´art i espai públic**
<http://2018.usbarcelona.com/>
-	**CAN BATTLÓ: Bloc Onze**
<https://www.canbatllo.org/>
-	**LACOL: cooperative architecture**
<http://www.lacol.coop/>
-	**QUI SOM: Ara més que mai volem ser barri**
<https://quisom2014.wordpress.com/>
-	**AUTO-MÀTIC: Machinic Protocols** Exposició 29.06 - 07.10.2018 en Arts Santa Mònica
<http://artssantamonica.gencat.cat/ca/detall/auto-matic>
-	**REHOGAR: Disseny Obert i Reutilització** - 21 de Setembre al 28 d’Octubre de 2018 en Espai Zero
<http://ajuntament.barcelona.cat/centredart/ca/content/espai-zero>
-	**TransfoLAB BCN: center for trash investigation**
<https://www.transfolabbcn.com/>
- **RECETAS URBANAS:** self-construction / recycling of buildings / collective and collaborative architectures / complex settlements
<http://www.recetasurbanas.net/v3/index.php/es/>

![]({{site.baseurl}}/week1-imga.jpg)

![]({{site.baseurl}}/week1-imgb.jpg)

![]({{site.baseurl}}/week1-imgc.jpg)

![]({{site.baseurl}}/week1-imgd.jpg)
*<https://brotherad.com/barcelona/>*
