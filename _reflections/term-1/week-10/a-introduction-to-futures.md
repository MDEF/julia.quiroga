---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---

#  The three tomorrows

I’ve decided to consider the idea of the future as a plural construction, as many facts and trends that combine with and influence each other.
Evolution has been going forward through time at different speeds. Visionaries have been moving into the present, understanding, criticizing and designing from their reality.
It seems that these movements have been more radical in the past centuries, that in ten or fifteen years, trends were changing by leaps and bounds and nowadays they are moving in a more *‘familiar and static sense, as a continuity of present’*
Considering the fact that we have today all the facilities, as a hyper-connected society, the approach to the future is in a way more responsible, methodic and cautious. But as a contradiction, this hyper-connected reality lead us to an unmeasurable and expansive impact when we consider size and simultaneity.
The linear effect that changes may have had in the past has been modified to a network of impacts. The potential of escalate that each event might have is unpredictable and unknown, and many times frightening.
But I keep on believing that the seed for this future phenomenon is more limited and restrained. As if we were denying the #blackmirror future and consequences.

This reflection leads me to another question that concerns the idea of future and if this is the same for everyone. If we consider our future scenarios from our extended present, we should find different futures for each society or group of people. Our capability to design the future from what we experience at the present time might not be the same for each person worldwide.
Our particular worldviews limit the perception of the future for us. So the future for Third World societies is not the same than the future for First World cultures. That’s why I encourage myself to understand each background, study their present and the way they reach this point in time, and from that knowledge I'm able to design a stroke to their own future. Without leaving behind the fact that we have to force and push the boundaries of our comfort zone but without taking care of the human and sensible evolution of the species. *We need to take account of empirically observable trends, theoretically understand the mechanisms that produce them, and incorporate as much imagination and creativity in the whole exercise as possible.*
For this approach I’m still recreating the future at its first level or its first tomorrow, as an EXTENDED PRESENT; a future that has already been colonized.

If I have to reflect on the others’ future, I find myself encountering the FAMILIAR FUTURE, the one that somehow takes advantage of globalization. Because it works with images and imaginings of the future, from popular *‘futurology’ and science ﬁction novels, ﬁlms and television shows.* Here is where novelty is introduced as an emergent event that can have potential. This stage of the future works in a singular manner, but also includes the plural scale. Its individual approach comes along with the idea of finding what’s familiar in the range of complex possibilities, and its plural sense lies in the idea of engaging alternative and divergent imaginings.
The last or third tomorrow is the UNTHOUGHTFUL FUTURE, which lies beyond our imagination and outside our worldview assumptions. It invites us to the idea of chaos, *to something outside the framework of conventional thought—something that does not allow us to focus on or think about it.*

![]({{site.baseurl}}/threeT.jpg)

### The Three Tomorrows of Postnormal Times$ Ziauddin Sardar, John A. Sweeney (2015)

Each tomorrow has a particular type of ignorance attached to it. *When complexity, chaos and contradictions come together, it should not surprise us that uncertainty is the result. The most basic variety of uncertainty emerges when the direction of change is known but the magnitude and probability of events and consequences cannot be estimated.*
In the Extended Present we find unpredictable events that people attempt to ignore, even though they are widely predicted by experts. These WHITE ELEPHANT are a sort of known and unknown events, which capture the post normal dynamic of this first future.
In contrast, over the Familiar Futures, we have the BLACK SWAN events as imperceptible actions. They are totally outside and way beyond our observations and somehow they might be positive, *which is to say that their impact might illuminate previously unimagined opportunities.* Indeed, it has been argued that Black Swans are responsible for some of the greatest societal changes of history.
At the end, we find the BLACK JELLY FISH as these things we think we know and understand but which turn out to be more complex and uncertain than we expected. These events are in the Unthoughtful Future and they make us consider the huge impact that little actions might have on larger scales. It shows us the interconnection, network, complexity and contradiction.

![]({{site.baseurl}}/dali01.jpg)
![]({{site.baseurl}}/dali02.jpg)
### Salvador Dalí - Gitano de Figueres (1923) / Visage du Grand Masturbateur (1929)

In this line, I would like to understand if there is some kind of connection between these unpredictable events in the way and time they occur. I understand that any kind of White Elephant event is predictable and responds to the present scenario, but also I observe that Black Swans are necessary events for history and evolution, as these big and substantial steps. In other hand we are moving towards the kind of Black Jelly fish actions with a potential of scaling and interconnecting that goes beyond our daily perspective. As a conclusion, I've found fitting our actions and interventions very interesting and taking this into account, so that we know how to make a difference or keep on moving forward or just to seed an action for our future network.

![]({{site.baseurl}}/Afghanistanstability.gif)
