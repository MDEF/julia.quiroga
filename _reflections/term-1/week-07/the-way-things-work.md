---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---
#FLOW CHART

What happens when you understand the way things work?
There is a logical way you can resolve any problem or situation you want to reach. Beginning from the **BASIC** and starting point and knowing where you want to get, what’s your **GOAL**, these are the two minimum stages that you need for designing any diagram.
That´s how many things, most of them, finally work. From a telephone or printer machine, to something more complex as a circuit including an Arduino microprocessor. Is amazing to catch the template of it all, and just readapt it to what's our current project. You can also, transfer it into a DIAGRAM of ideas or into a conceptual map.

![]({{site.baseurl}}/FLOW.png)
![]({{site.baseurl}}/TELEFONO-ilovepdf-compressed.jpg)
![]({{site.baseurl}}/MAPME.png)

The INPUTS from where your Starting Point nourishes can be Digital Read or Analogical Read. Many of them are **SENSORs** we have all sorts of these. Synthetic Sensors, Capacitive Sensors, Multipurpose Sensors (as cameras with image recognition), Light Sensors, Proximity Sensors, Temperature Sensor, Biometric Sensor (where we can specify muscles/contraction sensor, cardiac sensor, encephalogram sensor)
In the other hand we have the OUTPUTS, which can also present in Digital Write or Analogical Write. And they behave as **ACTUATORs**. The most common are the Light Actuators, as led’s stripes or electroluminescence material, different Sound platforms, as transducers or speakers and also different Motors and Pneumatics machines.

![]({{site.baseurl}}/BALLOON.png)

For this particular week, we decided to focus on the OUTPUT: **EXPLODING AND LIGHTING A BALLOON**.
We have connected a **VALVE-ANALOG WRITE**, for supply air to the balloon, to a **RELAY-DECISION** (SRD-05VDC) that worked with the multiprocessor **ARDUINO-INPUTS**. The relay has been attached between the valve and the power supply, so when the inputs were in a *predetermined range*, the relay would close the electrical circuit and give a positive answer. This allows the valve to start the **PROCESS** of inflating the balloon.
We decided also to give LIGHTS to the balloon as another **DIGITAL WRITE**, with the same logic and scheme.
By the end of the week, we have changed our *predetermined range of INPUTS* to another **DIGITAL READ**. Through a board linked to WIFI (ESP-8266), and with a **MQTT broker**, we achieved a central communication with all the others projects in the classroom and from where we linked up to another inputs.

## VIDEO BALLOON 
