---
title: 08. Be Your Own System
period: 19-25 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

#WHAT IF … 
This  week we wOrked on several and speculative ideas. We designed situations, artifacts, desires and magic objects.
We envisioned a powerful future where we can carry out projects in another manner.
*What if we communicate in a different way?*
*What if I can make happy sensations come over and over again?*
*What if I can stretch the distance in our relationships?*
**What if… ?**
Each of these questions was asked and also was answered during the week.
The concept of Living with Ideas became more and more real. I experienced the awareness of new ways of dealing with my own ideas, to make them alive and to give them a place in the immaterial/material actuality.

#W1
##MATERIAL SPECULATION
We carried out an exercise imagining a possible and near future, where some concepts and realities might change and we, as designers, should give solutions.
We designed things that are important due to the experience of living with them, instead of the object per se.
*“Create an object for a possible future, live with it and learn about it as much as possible”*
This was the proposition, to learn about the future in this present, from the insights of our SPECULATIVE MATERIAL.

- *What if we communicate in a different way?*
We are used to enclose our ideas, our meanings, our own concepts and profiles in a visible and graphic language. In this century we are giving more and more priority to information in a visual platform. Sometimes tactile sensations come as inputs, but not in a relevant way. And the manner in which we introduce each other to the rest derives from this structure and pattern. We hand someone a business card, for example, and we pretend to be known through our webpage, from the story we have decided to tell about our works/projects/relevant experiences and skills. It is here where I would like to invite myself to think about a near and possible future, where we recapture some other senses.

![]({{site.baseurl}}/businesscard.jpg)


We have sO many other ways to communicate about ourselves, that we have decided to simplify. The human structure of thinking and making relations is not at all simple, sO I would like to invite us to show that complexity in order to enrich our interactions.
That’s how I have designed a new and interactive presentation card. A (speculative) material that you have to run through, you have to perceive and you have to translate. While you experiment with this new format, you will find out some meanings for yourself and for the other; so the communication becomes something real and between both participants, considering the place and the time as well. Nothing is left behind, all the insights are taken part, and this is the way I think us, as complex individuals, relate with others and with the environment. It is just a matter of transfer the interdisciplinary process to the communication level.

![]({{site.baseurl}}/CEPILLO.jpg)
![]({{site.baseurl}}/IMG.jpg)

This exercise helped me to visualize my DATA FRIENDLY idea, to realize how materials can also express and give us information. I was immersed into the idea of DATA as a digital platform, but this experience helped me to engage in and to work in a language that, nowadays, is more familiar with my reality. From this stage I can express the same idea and keep on growing exponentially. To create new forms of communication and new ways of interaction, shall ask from myself to be aware of the feedback, of the new body language, of the new gestures each individual can experience and create; looking for the potential of people’s real feelings and reactions.
**How do we express information?**

#W2
##TAKING A PERSONAL PERSPECTIVE
This exercise challenged us to USE our project. A real and first experience of what we would like to achieve and communicate to the world. It’s an unusual way to put your ideas on track, and it gives you another point of view that leads you to the starting point. Instead of thinking about a concept and working out the way to materialize it, this experience invites you to just WEAR it and to interact in real time with it, taking the challenges and inputs that come along.

- *What if I can make the happy sensations come over and over again?*
Some possible answers appear before me. But it’s important to be well informed of the context, the insights that you can take from this approach. Here again I would like to bring back the **5W + 1H**, because it is important to be aware of **What** we mean by happy sensations, if it is something that we can share with each other, that we can consider a common aspiration /**Where** and **When** we would like to feel them, thinking as something that we can activate in a conscious mode or perhaps our body language sends out that kind of desire because of our chemical behavior by biometric sensors / **Why** we want to feel these sensations over and over again, if it is for sure that we would like to be all the time in a happy mood / and at last (but not the least) for **Whom** are we designing it in a future.
The **How** of the experience would be the technological matter, where I firmly emphasize that we should also incorporate the insights. The understanding of the application is something that we must use in our favor. The inputs that the use of a technological device can offer us and, how they can change and feed our project is a feedback that I consider as important as the first and fundamental idea. The way we arrive at some goal it is as relevant for our project as the goal in itself. Our plan can and must change during this path; it must consider new perspectives, add new variables, be open to different reactions and keep learning and growing from them. As Machine Learning, that trains itself by examples, this project would be trained by the experience; gaining new tools from the Machine platform.

![]({{site.baseurl}}/descarga.png)
![]({{site.baseurl}}/COLOR.jpg)
![]({{site.baseurl}}/WEAR.jpg)


#W3
##MAGIC MACHINE
Saving as the need to accumulate something
The first time I came across this definition I was totally surprised, just because I dOn’t agree at all with it.
I’m against accumulating things, I think we already have too much of everything and instead of making and having more, I define saving as the action against accumulation. Saving for me is gaining more from what we already have. It is trying to discard things when we cannot obtain any more from them.
That’s why I decided to wOrk with this concept, to bring out the idea of less is more.
But also as we reduce material things in this world we can also reduce the distance we have between people. The same idea in a different platform and purpose: the idea to create a MAGIC MACHINE that can save water, for example, also can be used (after some modifications) as an optical device that makes social contact more focused. Because when we define Social Contact as the need for relationship with others, we can observe that relations today get lost with all the interactive and visual distractions we have. We talk to someone in particular and, the near environment, our surroundings and our parallel reality (our collapsed mobile) grasp our attention.

![]({{site.baseurl}}/TIME.jpg)


- *What if I can stretch the distance in our relationships?*
It is a matter of focusing or it comes beyond that? I do not think that the problem will be fixed up with an optical device, but it is clearly a starting point. And if not, it is a way to notice how near we are from solving something that has become so big and incommensurable. I know that many psychologists are working on this field of focusing and mindfulness, as the process of bringing one's attention to experiences occurring in the present moment, now and here. If we could detect all the information surrounding us, if we can pay attention to that THIN DATA that sometimes we disregard, for sure some relationships and some interventions in the physical world would be different.

![]({{site.baseurl}}/Mindfulness.png)

**Would it be possible or even imaginable to focus our attention on just one person or relation at a time? This would give us MORE information about that unique individual or interaction?
-LESS DISTANCE MORE INFORMATION-
This will help us to create better relationships with each other?
This would make us better citizens, better social beings?**


![]({{site.baseurl}}/SAVING.jpg)
![]({{site.baseurl}}/SOCIAL.jpg)
![]({{site.baseurl}}/FOCO.jpg)



At the end of the week I decided to work with one of the strategies we have learned:
1.	Creating a SPECULATIVE ARTIFACT
Imagining a future where our language and way of communicating with each other reaches features of reciprocity, of real meanings, of natural behavior and true desires was the goal.
I decided to get deeper in the alphabet that we use to express ourselves and compared it with other ones, for example, the Arabic. I found out that the Arabic language is written in another way and has a particular behavior regarding letters and their combination.
We have some social letters (if we can say so), which change their graphic design if they are in combination with one or another part of the word. One letter has a drawing for its isolated use, another drawing when it’s in the middle of a word, and if it’s at the end or at the beginning. So we can think these letters, as contextualized letters, that are partially modified because of the surrounding context. A very interesting concept if we transfer it to human behavior in another scale.
-We can find a similar example of this approach in the typeface UNIVERSE, designed by Adrian Frutiger. He propose UNIVERSE as a typography that goes beyond the quest of designing individual letters, attempting instead to design SPACE, to create a system of relationships between different sets of shapes which share distinctive parameters.-

![]({{site.baseurl}}/ARABIC.jpg)


I’ve tried to write down a simple phrase: **happy cities (are) collaborative friendly experience**, by choosing the letters in the Arabic alphabet by its drawings, instead of by its meaning. While I was choosing these letters and combining them I realized they were changing in the assembly. So it became an exercise of going from the parts to the whole, trying to fit my idea of HAPPY CITIES into a global and particular graphic. Once I went through this exploration I have decided to finally translated the phrase, and found out what was the real (and Arabic) meaning of my words.

![]({{site.baseurl}}/LANGUAGE.jpg)


The translation of my sentence gave me new inputs, which clearly indicates the distance between both alphabets, between the ways of communicating in a written language and by the end the distance between cultures. I found out that there are gaps in the middle, which I can use as opportunities to work on, and that I can take as the workspace for my project.
I decided to embrace these variables and to work with them in order to understand where are we coming from and start drawing the new path of intervention from that knowledge.
