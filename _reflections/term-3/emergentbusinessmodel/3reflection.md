---
title: EMERGENT BUSINESS MODEL - FINAL REFLECTION
period: 1-3 April 2019
date: 2018-10-06 12:00:00
published: true
---
## EMERGENT BUSINESS MODEL


The information we were able to work with during these classes was very interesting and useful to place our intervention in the present world.
First of all, it was the first time that we could explain our project in a few words and in one sentence. This is something to take into account for expressing a bunch of ideas that we were managing during these past months.

![]({{site.baseurl}}/TWEET.png)

After this first step, the perspective projected to us by these classes was very useful to understand the real **IMPACT** we were expecting from our project and the solution that we were aiming to give.
From my perspective, the project doesn’t have to be the answer to all the questions. Instead it could give some kind of impact either as a *Product/Service, or Develop a Capacity, or Develop Knowledge, or Develop a Movement/Behavior, either Develop an Infrastructure or Promote a Legislative Change*.

![]({{site.baseurl}}/IMPACT.jpg)
![]({{site.baseurl}}/LEVELSIMPACT.jpg)

Taking this perspective as a starting point, it did relieve me as regards my project, because you frame your intervention in another existing context: being a part of another and bigger system, where you can offer a good service or improve some of the already existing services.
This also made me realize that, for the purpose of the impact itself, it is another superior system. The impact doesn’t happen just by one or two isolated interventions; it is framed in a map of different levels, where many systems regroup and generate a systemic change, which later on could end in a paradigm’ change. It’s a matter of scale; and how your project will help one or several parts of this structure.
In consequence, your intervention can take care of an immediate and specific need or work beyond a regional demand. Also it can address a social problem or it can change the way a society thinks about something; the latter, at the major scale.
From this point of view, designing an intervention is something more achievable and realistic to attain. Framing your project within a system that supports and reinforces your ideas or points of view and, trying to change one part of it, instead of thinking in changing the whole, gives me a relief and also took some weight off me, in relation to my expectations. I suppose this is something that every designer struggles with during the designing period and when you make things happen. In my personal experience, working with and for a system, helps me to spread my ideas faster and rationally.
Also you begin to understand that there are many other projects that are working or made an impact within your field. For me was encouraging and motivating to find some referents that had been working on my field of interest and what part of the system they have changed. In that way I took them as partners within the systemic change and towards the paradigm transformation.
One of them had made an impact in the way architects and clients get related, offering a service [GEHL PEOPLE](https://gehlpeople.com/); the other one, had made an impact on the citizen’s role and how as they got rights they also got responsibilities towards the city, developing a behavioral change [JANE JACOBS](https://centerforthelivingcity.org/janejacobs); and the last one had made an impact in the way users (as citizens) can change their relation with the data and participate altogether in a creative action, strengthening a network [R/PLACE](https://www.reddit.com/r/place/)
Discovering these partners helped me to picture my context and to evaluate the efforts that I needed or not to put into the project. We had been taught along these classes about the idea of **Minimize the Efforts**, both external and internal. And not only because of the context, but as regards our project: *What is the essential ‘product feature’ in my project? What can I take advantage of, that already exist in the field of action?*
For my particular intervention there are many things that are already implemented and that the citizens already adopted, in consequence there is no need to start from scratch or to design the path from cero. On the contrary, I’m taking advantage of how citizens (users) already interact with digital devices and digital platforms, and translate the data that they have already provided to public and design benefits.
The last important input from this workshop was the introduction of the **Pentagrowth: The five levels of exponential growth**:

![]({{site.baseurl}}/PENTA.jpg)

In my perspective, the most interesting topics of this scheme were to *Empower the Users* and to *Enable the Partners*. These two variables were not taken into account in my design process and it is something that is very useful to consider.
On the first level, it was very clear to see how the *Users could become Producers*, and how this path would take my project to an exponential growth. My intervention's goal is to generate the environment for the users to help them to interact as well as to work with the capabilities that these users can offer.  
The other level of this scheme that surprise and made me reconsider, was to *Enable the Partners* instead than creating a competition with them. To connect with different companies and to generate a decentralized ecosystem, it’s a new perspective and a new approach for encouraging the horizontal workflow. To compare yourself with the others that are working in the same field and to understand the place that each one is taking in the impact map, helps to gain ground and to be able to make a difference. United we can do more than individually, combining existent resources and generating bigger impact.

![]({{site.baseurl}}/NEWS.png)
