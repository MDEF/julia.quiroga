---
title: MATERIAL DRIVEN DESIGN - SPECIAL ASSIGNMENT
period: 1-3 March 2019
date: 2018-10-06 12:00:00
published: true
---

**HAIR** is a protein filament, being alpha-keratin the primary component, that grows from follicles found in the dermis. Hair is one of the defining characteristics of mammals, but also is an important biomaterial.
There are two distinct structures in the hair: the part beneath the skin, known as the hair follicle. This organ is located in the dermis and maintains stem cells, which not only re-grow the hair after it falls out, but also are recruited to regrow skin after a wound; and the hair shaft, which is the hard filamentous portion that extends above the skin surface.
Each hair shaft is divided in three zones: the cuticle, the cortex and the medulla.

- The cuticle is the outer covering and it´s a single molecular layer of lipid that allows the hair to repel water. It´s structure is made of several layers of flat and thin cells overlapping one another.

 - The cortex contains keratin and melanin, this last one colors the fiber. Eumelanin is the dominant pigment in brown and black hair, while Pheomelanin is dominant in red hair. The blonde hair is the result of having little pigmentation. This zone is highly structural and organized and is the primary source for mechanical strength and water uptake.

The shape of the follicle determines the shape of the cortex, and the shape of the fiber* is related to how straight or curly the hair is. People with straight hair have round hair fibers. Oval and other shaped fibers are generally wavier or curly.

- The medulla is the innermost region. Is not always present and is an open and unstructured region.

*[*fiber is a natural or synthetic substance that is significantly longer than wide. Fibers are often used in the manufacture of other materials.
Natural fibers can be classified according to their origin: Vegetable fibers / Wood fiber / Animal fibers / Mineral fibers / Biological fibers]*

A particular fact of the hair, it´s related to its growth. Hair growth begins inside the hair follicle. The only ¨living¨ portion of the hair is found here. The rest of the hair, the visible shaft, has no biochemical activity and is considered ¨dead¨.

For mammals in general, the most important functions of the hair are thermal regulation and camouflage, as a defensive function for some. It has also a sensory performance, extending the sense of touch beyond the surface of the skin.
For humans the hair found on the head serves primarily as a source of heat insulation and cooling.

Some important properties of the hair are:
-	**is a completely biodegradable and renewable material**,
-	it has a slow degradation rate,
-	**it has a high tensile strength**,
-	**it is light weighted**,
-	**it has elastic recovery and a scaly surface**,
-	**and it has unique interactions with water and oil**

Every material has several sociocultural and economic aspects associated with it, and they often determine the adaptability of the use or the technology within it. Developing appropriate utilization for human hair waste, within a context, requires the consideration of all possible uses and technologies along with their socioeconomic and environmental impacts.

Hair main properties plus its socio-cultural aspect led to many diverse uses of its waste. These uses also depend on the variety of the hair available, varying in terms of five parameters: length, color, straightness or curliness, hair damage, and contamination.
Another consideration to think of human hair is that as a natural resource will be increasing in the future since the world’s population is rapidly rising and will causes several problems for both the environment and human health, if its waste is not controlled. Nowadays most of the waste ends up in landfill or slowly decays in the environment, releasing toxic gasses and choking the drainage system. Although hair is a biodegradable material, the biological decomposition pathways take a few months.

There is a huge market related to the textiles and fiber stuffing industry, and I found several examples of this use that made me take into consideration this raw material, the possibility of reducing its waste and its inner properties as well.
One of the most common uses is the **YARN** of hair. Taking advantage of the tensile strength and light weighted properties above the others, human hair has been used to make ropes in many cultures, for example, to lift heavy beams in the construction or for domestic purposes. A rope or a cord consist of a bundle of fibers (yarn) The tensile strength of a rope comes from the strength of individual fibers and the interfiber friction between them. The inter-fiber friction also prevents them from slipping past one another.

**THE NEW AGE OF TRICHOLOGY – by Sanne Visser
Harnessing the potential of hair**
[THE NEW AGE OF TRICHOLOGY](http://www.sannevisser.com/)

Sanne Visser is a Dutch designer who works and lives in London.
A material explorer and maker, graduated with the masters Material Futures at Central Saint Martins. Her main interest as a designer is on material innovation, sustainability and future thinking. The production process is highly important in her work, where craft meets innovation and system design.

The New Age of Trichology created a method of producing a material by using other existing crafts and using a waste stream. In this case focused on the tensile strength of the human hair fiber, where spinning and rope making techniques are applied. The system is a closed loop system in a way that the raw material collected at the beginning remains additive free and can go straight back into nature at the end of its life cycle, through composting or recycling.

On average, one human hair can hold up to 100 grams of weight, depending on the person’s diet, health, environment, ethnical background and treatment of hair.
The process of making a continuous, multifilament yarn from continuous or staple fibers is called spinning. The fibers in a yarn are held in place through the radial frictional compressive force generated by the twist that is given to the yarn. When such a twisted yarn is pulled in tension, the individual filaments are forced together and the load is shared evenly by the multiple filaments in the yarn. The frictional force increases with the angle of twist.
In effect, some of the important yarn structural parameters are: Linear Density / Number of Fibers / Twist in the Yarn

![]({{site.baseurl}}/TRI2.png)
![]({{site.baseurl}}/TRII.png)
#### Yarns converted into ropes by adding different accessories for diverse uses (such as shoulder straps, bungee cords, water bottle holders and large bags)

Human hair has sufficiently high strength for use as a **SUTURING MATERIAL** in surgery. It is relatively easy to tie knots with and is non-infectious (because of its slow decomposition rate and high compatibility with the human body) Using hair as suture was known in Europe in the middle ages. Could be easily procured and there was no additional financial burden on patients. Also it could be easily sterilized.
Hair as suture material was accepted by host tissues and the healing response and wound union was excellent. Women's hair was found to be more suitable to be used as suture material than men's hair. While men's hair was usually short, thicker, coarser and darker, women's hair was found to be longer, thinner and finer.

![]({{site.baseurl}}/SUTURING.png)
#### Facial wounds are a challenge for dermatologic surgeons, particularly traumatic facial wounds, because they can leave disfiguring scars. To obtain good results, narrow needles and sutures are needed. Hair filaments have a very small diameter (0.06-0.1 mm) and could serve as suture threads for facial wounds.

On another note, we can focus on **FABRICS**.
A *textile* is a flexible material consisting of an interlocking network of natural or artificial fibers (yarn or thread). A *fabric* is a material made through weaving, knitting, felting, spreading, crocheting, or bonding that may be used in production of further goods. *Cloth* is often a piece of fabric that has been processed.
Animal textiles are commonly made from hair, fur, skin or silk.
Wool, for example, refers to the hair of the domestic sheep or goat, which is distinguished from other types of animal hair, because each individual strand is coated with scales and tightly crimped. Also is coated with a wax mixture known as lanolin (sometimes called wool grease), which is waterproof.
As we mentioned earlier, there are several techniques for producing a woolen fabric. I would like to focus on the *felt* process, just because it is the most common technique used for human hair, using it as a raw material.

Felting is obtained by matting, condensing and pressing fibers together. Felt can be made from natural fibers such as wool or animal fur. Felt from wool is considered to be the oldest known textile.
Felt making is still practiced by nomadic peoples in Central Asia, where rugs, tents and clothing are regularly made. In the Western world, felt is widely used as a medium for expression in both textile art and contemporary art and design.

There are three manufacturing methods for the felting process: Wet Felting, Needle Felting and Carrot Felting, this last one is not used due to its toxic components’ emanation during the procedure.
In the Wet Felting process, hot water is applied to layers of animal hairs, while repeated agitation and compression causes the fibers to hook together or weave together into a single piece of fabric. Wrapping the properly arranged fiber in a sturdy, textured material, such as a bamboo mat or burlap, will speed up the felting process.

Only certain types of fiber can be wet felted successfully. Most types of fleece, such as those taken from the alpaca or the sheep, can be put through the wet felting process. These types of fiber are covered in tiny scales, similar to the scales found on a strand of human hair.
Heat, motion, and moisture of the fleece causes the scales to open, while agitating them causes them to latch onto each other, creating felt.

Crimp in the fibers, natural or artificially created, helps in interlocking the fibers. Crimp in a fiber can add some bulkiness to it. Natural fibers such as human hair have scales and sometimes a natural crimp that make interlocking of fibers easy.

![]({{site.baseurl}}/HAIR MIC1.png)
![]({{site.baseurl}}/HAIR MIC3.jpg)

**WARMTH – by Zoran Todorovic
Serbian Pavilion, 53rd Venice Biennale, 2009**
[WARMTH](https://www.zorantodorovic.com/portfolio_page/warmth/)

Installation / Action / Video
1200 square meters’ human hair felt, of which 456 pieces 80x120cm and 327 pieces 160x120cm numbered, one square meter being produced out of approximately 200 people, which makes a total of approximately 240 000 people.

The project Warmth was for the first time shown in the Serbian Pavilion at the Venice Biennale where the exhibition format is at the same time the format of national presentation.
The project includes the process of systematic stockpiling of human hair. The hair was collected for months in hairdressing salons where it was cut off voluntarily according to personal desires as well as in military barracks. This *biowaste* was recycled as material for fabrics looking like blankets or mats and the conditions in which this process took place were thoroughly documented; namely, recordings were made of all the production stages (haircutting, collecting, storing, cleaning, steaming, felting, cutting) Also carefully documented was the complex organization of this work that included a number of collaborators and factory production. Final felted products were folded up and packed into standard bales ready for transportation, export, exhibition, use or examination.

![]({{site.baseurl}}/FELT.jpg)
![]({{site.baseurl}}/FELT1.jpg)

**COOPERATIONS & EXCHANGE
Alix Bizet & Aleksandra Lalic & Zoran Todorovic – hair_dress**
[Aleksandra Lalic](aleksandralalic.blogspot.rs)

Alix Bizet, a French student graduating at the Design Academy Eindhoven in The Netherlands, designs clothes made of human hair. *HAIR MATTERS* is a graduation project, in which she collected and examined human hair from African American hairdressers to create jackets and hats, developing a special weaving technique.
[Alix Bizet](http://alixbizet.com/exchange)

![]({{site.baseurl}}/FELT2.jpg)
![]({{site.baseurl}}/FELT1.png)

Another possible use for human hair is **REINFORCEMENT** for Construction Materials
Compressive strength is the primary measure of the strength parameter of concrete. It depends on the quality of the materials used to prepare the concrete (cement, fine and coarse aggregates). As technology is constantly advancing due to commercialization, new discoveries are being made to explore the possibility for increasing the compressive strength of the concrete within an economic environment. Human hair offers resistance to tension and there is also plenty in nature, so it can be used as a fiber reinforcement in concrete.

Human hair reinforcement offers higher tensile strength which can be equal to the tensile strength of copper wire. It has the same diameter and also a cortex which is made of parallel fibrils and a corresponding matrix. The way these two parts work together is what helps the hair to endure stress and strain. They undergo structural change when hair is stretched before the actual breaking occurs. Hair can be stretched 1.5 times of its initial length before breaking. Human hair not only offers higher compressive strength but also reduces the micro cracking and increases structural stability. Also hair fiber has an elastic characteristic, and it may undergo moderate stretching either wet or dry. When dry, the hair thread may stretch 20-30% of its length; and, in contact with water, this may reach up to 50%. Research shows that also enhances the thermal insulation capacity of the clay structures.

The concrete reinforced by fiber is substantially lighter as compared to heavy plain cement concrete but it offers the equivalent strength making itself more workable. Fibers which are present in concrete reduce internal forces by blocking the formation of microscopic cracks within the concrete. The hair fiber reinforced concrete is used in the laying of runways, aircraft parking pavements and other type of pavements. It is used for lining of tunnels and rock stabilization. In the preparing of spherical domes, thin shells, walls, pipes and manholes the fiber reinforced concrete is used as well.  The fiber reinforced concrete is used for the construction and repairs of dams and other hydraulic structures to provide resistance to cavitation and erosion caused by the impact of large waterborne debris.

The use of fibers has an old history like use of horsehair and straw in clay for making floors and bricks, and after steel fibers came into practice as well. After that, many researchers have been working to figure out the use of such fibers for improving the core strength of the matrices all over the world.
In recent decades, a large amount of domestic and industrial waste has been observed which is posing a great problem for environmentalists around the world. Disposing such large amount becomes a significant problem, and such waste product can be very well used as a fiber in concrete which will not only reduce problems of environmentalists but will also reduce utilization of other important materials like aggregates and cement.

![]({{site.baseurl}}/CONCRETE2.png)
![]({{site.baseurl}}/CONCRETE4.jpeg)
#### Fiber-reinforced concrete (FRC) is concrete containing fibrous material which increases its structural integrity. It contains short discrete fibers that are uniformly distributed and randomly oriented. Fibers include steel fibers, glass fibers, synthetic fibers and natural fibers.

Another hair characteristic is the unique interaction that it has with water and oil. This property makes possible to use human hair as an OIL – WATER separation or as a **FILTER**.
Human hair surface has a high affinity for oils—much higher than its affinity for water. This property is very useful in oil-water separation. Booms and mats of human hair have been used to clean up coastal oil spills in the Philippines and the USA, for example*. In this method, oil can be recovered by wringing out the hair, which then can be reused up to 100 times -advantages not found in other oil spill remediation methods. With this method, up to 98% of the spilled oil can be recovered. The oily hair can then be used to grow oyster mushrooms, which decompose the oil. The hair then left can be composted.

**MATTER OF TRUST – by Lisa Craig Gautier
Clean Wave program**
[MATTER OF TRUST](https://matteroftrust.org/history)

In the 1990s, Lisa Craig Gautier had heard about a hairdresser in Alabama called Phil McCrory who first suggested that hair was a really good resource and started collecting it in his garage.  
*“You shampoo because your hair collects oil, and hair is a renewable resource growing right in front of our eyes.”*
She was inspired by this phrase and took a step forward, bringing hair to even be able to absorb oil, like other renewable natural fibers such as feathers, fur trimmings and fleece. Through *Clean Wave* program, hundreds of thousands of green salons, pet groomers, farmers and many other donors helped to collect hair, fur and fleece clippings and to create oil spill cleanup barriers.
The project focused on the possibility of finding a use for waste hair that could be used to clean up oil spills and that the oil could be recovered. As the process is ecofriendly and does not require any chemicals, it may lead to the development of a new technique of separating oil water emulsion, which is simpler.

![]({{site.baseurl}}/MATTER.png)
![]({{site.baseurl}}/MATTER3.png)

Since water is taken in larger concentration than oil it is more probable for water to be absorbed in larger quantities, but when the experiments were carried out, the reverse was found to be true as well. Moreover, when the phenomenon was studied under optical microscope it was observed that oil replaced water from the hair surface.
It can be explained in terms of selective physical absorption. The adhesive forces between oil and hair are greater than those existing between water and hair. Therefore, in the presence of oil and water, hair selectively absorbs oil. Thus, oil is separated from water when a mixture is passed through a bed of hair. The absorption might be taking place on the glassy membrane, a non cellular portion.
To explain the recovery of the oily hair afterwards, the multi-layered structure of the cuticle, covered in tiny scales, makes a formidable barrier to penetration of materials into the interior of the hair.  

Human hair can also separate emulsified oil in water, which is very expensive to clean by other methods. This property can greatly help in cleaning effluents from industries such as oil refineries.
Tightly woven human hair cloths were used in the 1920s as filters for heavy oils in refineries and distilleries because these processes involved high pressure that many natural fibers could not bear.
One more application for human hair waste is in the agriculture field, more precisely in **FERTILIZERS**.
Human hair is one of the highest nitrogen-containing (16.1%) organic material in nature because it is predominantly made up of (nitrogen-containing) proteins. In addition, human hair also contains sulfur, carbon, and 20 other elements essential for plants. In the atmosphere, hair decomposes very slowly, but moisture and keratinolytic fungi present in soil can degrade hair within a few months.
Recent experiments on horticulture plants show that direct application of human hair to soil provides the necessary plant nutrients for over two to three cropping seasons.

**SMART GROW**
[SMART GROW](https://smartgrowing.com/products/smartgrow-all-natural-growth-mat)

This company has popularized the fertilizer use of human hair in the USA by selling it in the form of hair mats for potted plants.
When applying fertilizer, the *SmartGrow* mat captures some of the nutrients that otherwise would filter through when watering or during rainfall. These nutrients will slowly be released over time. Also, as the mat slowly biodegrades, it releases its own nitrogen and other natural, essential nutrients. The final result is a reduction in the amount of fertilizer required.
During watering, or when it rains, water enters the soil and saturates the hair fibers. *SmartGrow* acts as an effective water retention device. Water from the mat is released to the roots as the soil dries.
Lastly, the all-natural hair-fiber mat is a natural pest deterrent to many animals, including deer and rodents.

![]({{site.baseurl}}/SMART1.png)

For plants, the process of growing needs water, sunlight and nutrients like nitrogen. Although there is plenty of nitrogen in the atmosphere, it is not available in a form that is usable by the plants. Therefore, it consumes nitrogen in molecular form from soil. Therefore, to make any plant healthier a combination of nutrients must be supplied. Human hair-waste generated by barbershops is an example of an unused by-product that may have some application in agriculture. Hair-waste is usually put in the garbage or in compost and ends up either in landfills or at regional composting facilities. Because of its high nitrogen concentration, hair can be used as a feedstock for composting, as mulching material for weed control and as a nitrogen source for crop plants.
Human hair is also known to address problems arising from many animals as well as insect pests, although by different mechanisms. Among large animals, it has been used to repel rabbits, rodents, wild boars and deer. Typically, the hair is spread along the boundary of the fields/farms or near rat holes in the field.
Rabbits, rodents, and wild boars find their food by sniffing, and hair supposedly causes them discomfort during sniffing by coming into their nostrils. In the case of deer, repulsion is supposedly caused by human smell emanating from the hair because the technique does not seem to work well in the areas where deer are unafraid of humans.
Among insects, human hair is used for deterring rhinoceros' beetles. Small balls of human hair are placed at the nodes of the affected plant such as coconut trees. Beetles get tangled in the hair and became unable to move.

![]({{site.baseurl}}/HAIR RECYCLE.jpg)
#### Flow of human hair as material in various possible uses
