---
title: 10. MOLDING & CASTING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

## MOLDING & CASTING

This week, for the assignment Molding & Casting, we have worked in groups.  
I would like to clarify that I have worked in a group with [Silvia](https://mdef.gitlab.io/silvia.ferrari/fabacademy/casting/), and we divided the tasks to work on.
We did together the first part: designing and milling the MOLD, and then she did the CASTING part, because it was to toxic for my pregnancy condition.

The final product will be epoxy resin rings, that we checked out, as an inspiration, from another [previous FabAcademy student from Korea](http://archive.fabacademy.org/fabacademy2017/fablabseoul/students/385/week13.htm)

![]({{site.baseurl}}/REFERENCES.jpg)

The first thing we did was measuring our fingers for having the right dimensions and to modified them in the 3dmodel.
-	Julia: 53mm, 57mm (diameter 17mm, 18.2mm)
-	Silvia: 63mm, 58mm (diameter 20mm, 18.5mm)

## DESIGN PROCESS

We decided to prepare our MOLD by wax milling. We found an unused wax brick that suited us perfectly. Its dimensions were 14.5 cm X 8.5 cm X 3.5 cm, and our rings should fit into it without a problem.
Anyway we needed to smooth both faces before milling it, because the surfaces weren’t perfectly flat. One face for milling the MOLD and the other one to be smooth enough to stay steady at the machine.
The fast way to proceed for this task was with the CNC machine.
We had a pretty thick block, so to take out a total of 5mm from each side wasn’t a problem. We worked in RhinoCAM just designing a brick with the measurements a little bit bigger than the real one (width and length), and we decided to use a 6mm flat endmill to surface each face. We repeated the command two times on each face, surfacing 2.5mm each time.

![]({{site.baseurl}}/CNCFLAT.jpg)

After we had the final measurements of the brick 14.5cm x 8.5cm x 2.5cm, we began designing the MOLD in Rhinoceros 3D. The interface wasn’t so friendly, so we changed the software and started designing in Autodesk Fusion 360
We began by designing a rectangle with the same measures of the wax brick and then extruded it to the brick’s height. The process then required to:
-	Design a smaller rectangle inside and then lower it
-	Design circles for the rings, one internal – the size of our fingers, one external – the desired thickness of the rings (5mm)
-	Extrude them for the desired length of the ring
-	Lower the central part of the ring to ensure that the epoxy resin won’t spill out while casting

![]({{site.baseurl}}/3DRINGS.jpg)
[link to the file]({{site.baseurl}}/files/RIngdefmesh.3dm)
[link to the file]({{site.baseurl}}/files/RIngdefmesh.stl)


## MILLING PROCESS (with wax)

Once we had our final 3dMODEL and we had exported a mesh in .stl format, we were able to continue with the milling stage.
It was the first time that we milled in wax with a considerable thickness, instead than in a very flat piece of copper.
Some parameters in *FAB MODULS* had to be changed from our last experience.

The type of machine we are using is Rolland Mill
The process we are going through (WAX ROUGH CUT with 1/8 flat endmill and FINISHING CUT with 1/8 rounded endmill)

## WAX ROUGH CUT
In the **INPUT**: select the MESH (.stl) file
-	Units/in: 25.4
-	View z angle / View x angle: 0
-	View y offset: 0
-	View x offset: as is predetermined, for not moving to much the model from the origin
-	View scale: 1
-	dpi: 500
You *Calculate height map*

![]({{site.baseurl}}/FABMOD1.png)

In the **OUTPUT**: Rolland mill (.rml)
-	Name of the Machine: SRM-20
-	Speed (mm/s): 20
-	X0/Y0/Z0: 0mm
-	ZJog: 6mm
-	Z(home): > 6 (if is not predetermined)

In the **PROCESS**: WAX ROUGH CUT (1/8)
-	Direction: select CONVENTIONAL
-	Tool diameter (mm): 3.175
You *Calculate* and *Save*

![]({{site.baseurl}}/FABMOD2.png)


## FINISHING CUT
All the parameters and actions are the same, the only thing that you have to change is in the process stage.

**PROCESS**: FINISHING CUT (1/8)
-	Direction: select CONVENTIONAL
-	Tool diameter (mm): 3.175
You *Calculate* and *Save*

At the end you will have two different files for uploading in the milling machine.
We encountered several problems with this interface in relation to show us and calculate model.
-	The first problem was the orientation:
We oriented the 3Dmodel vertically, while in the milling machine the brick is oriented horizontally. We rotated the file in *Rhinoceros* through the *rotate2D* command, and saved it with the binary option, otherwise the file is not recognized by *FAB MODULS*.
-	The second problem appeared when we started milling, because the milling wasn’t starting in the middle of the brick as it should. Once we cancelled the job in the Rolland Machine, Eduardo helped us understanding what had happened, and in the process he indicated us to *Calculate* with a lower dpi resolution to make the overall process faster. The problem was that in *Rhinoceros* the 3Dmodel wasn’t in the origin, so we move it to it through the *Move* command
Once we fixed this issue, we open the .stl file again in *FAB MODULS*, started the final calculation, *Save* the file for the milling machine in .rml format (x2) and restarted the milling process.

In the Rolland Machine we worked with the *VPanel for SRM – 20* for determining the x,yhome and also the zhome.
This procedure was really easy to follow, just because it was similar to previous assignments.
The only thing to take into account was to change the mill between one file and the other, between rough cut and finishing cut.
For the first step we used a 1/8 flat endmill and for the second step we used a 1/8 rounded endmill.

Within the design process we made a little mistake as regards the width measurement, so one of the two sides of the mold ended up really thin, just 2 millimeters thick.
In consequence a small part of it was broken and we had to repair it with some tape.

![]({{site.baseurl}}/MOLD.jpg)

## MOLDING PROCESS (with silicone)

After having looked at the leftovers from our classmates in FabAcademy Lab, we realized that we only had left, for **Smooth-on Mold Max 15 T**, a silicone rubber compound in two parts. On the [product’s webpage]( https://www.smooth-on.com/products/mold-max-15t/) we found all the instructions for mixing the silicone correctly and for preparing our mold.

![]({{site.baseurl}}/PRODUCT.jpeg)

The process was as follows:
-       To measure the volume of the silicone needed with water volume: by pouring water into the wax mold, we understood that we needed a silicone volume of approximately a plastic glass.
-       To clean the wax mold with the air pump, to dry it completely
-       To spray the *release agent* on the wax mold - it needs to rest for 10 minutes before pouring the silicone –
-       In the meantime, we mixed components A and B of the **Smooth-on Mold Max 15 T**. The weighted proportion should be 10A:1B. So we put roughly the volume of component A, we already knew we needed into a plastic glass, and we weighed it. For 144g of it, we added 14g of component B and we stirred for 3 minutes.

![]({{site.baseurl}}/STIR.jpeg)

-       Afterwards, we started pouring the mix into the wax mold. Everything went smoothly but since silicone is very thick, we were worried about leaving air bubbles inside the internal holes of the ring molds. To solve this issue, we put everything (the silicone already in the MOLD) under the vacuum for 10 min using the FabLab vacuum pump.

![]({{site.baseurl}}/SILICONE.gif)
![]({{site.baseurl}}/VACCUM.jpeg)

-       Finally the silicone was ready to rest for 24 hours

![]({{site.baseurl}}/SILICONE.jpeg)

After 24 hours, the silicone mold was ready and taking it out of the wax was very rewarding.

![]({{site.baseurl}}/OUTOFTHEMOLD.jpg)

As we were inspired by the pictures of the references, we decided to cast the rings using Epoxy transparent resin and small components like dry flowers, gold paper, copper filaments, tea leaves …

The Epoxy resin we found at the FabAcademy Lab has very easy instructions:

- Resin should be mixed with a *hardener* of the same brand.
- Hardeners could be slow/medium/fast, and according to which one is used, the proportion of resin changes
- We use epoxy resin with a medium hardener. That means that, measuring by volume, we should have 3:1 proportions (3 parts of resin for 1 part of hardener)

![]({{site.baseurl}}/RESINCOMP.jpeg)

Once mixed, the resin heated up quite fast. We Googled it and found it was a normal behaviour:

![]({{site.baseurl}}/RESIN.png)

The process of casting was slower than we thought and we worked in three stages:
-       first, we casted a small amount of resin,
-       second, we put the components in the mold, and
-       third, we put some resin to fill it again

The resin was very difficult to handle without a proper extruder (like a syringe) and the components were so small that we managed to complete only three rings before the resin heated up and got impossible to handle.

![]({{site.baseurl}}/RESINPROCEDURE.JPG)

We left the three rings to harden and we tried to experiment also with natural resin that one classmate was using as well. Natural resin is so much easier to handle, it hardens very fast but the rings are very fragile and almost impossible to use.

![]({{site.baseurl}}/NATURALRESIN.JPG)
![]({{site.baseurl}}/NATURALRESIN1.JPG)

After 24 hours, the epoxy resin was hard and dry and we could check how the first three rings were. They were perfect and only needed some sanding before being ready to use.

![]({{site.baseurl}}/RINGSFINAL.jpg)
