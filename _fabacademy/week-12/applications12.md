---
title: 12. APPLICATIONS & IMPLICATIONS
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---
## APPLICATIONS & IMPLICATIONS - Intro to project

## What will it do?

This project is a digital interface for inspiring the actual methodology of co-creation in participatory designs for public spaces.
My intervention would be shown through an API (Application Programming Interface), that neighbors could login from any digital device. The *inputs* for participating will come from its internal sensors (Camera, Audio, Ambient Light and GPS) and the *outputs* will be given by a model of Machine Learning – with Image and Audio recognition, from a backend server.
These outputs will be the data to co-design a particular public space in real time in conjunction with all the other users on line. All users must be kept in sync with the same view of the current 3dmodel state, otherwise users with different versions of the board will have difficulty collaborating.
The API should be open and transparent, so the community can build on it if they choose to do so.

## Who has done what beforehand?

There are several platforms with the purpose of participatory design out there in the globe.

Locally we have *DECIDIM*, from the Ajuntament de Barcelona. This digital open-source platform helps citizens, organizations and public institutions to self-organize democratically at every scale. It works with frameworks, as SPACES and mechanisms, as COMPONENTS, that allow operations and interactions between the users.
[DECIDIM](https://decidim.org/)

*SUPERBARRIO* from Master in City & Technology at IaaC (2017). This platform allows citizens to visualize and navigate in the 3D model of the neighborhood. The methodology is to interact with the space, allocating already designed modules or assigning a function to empty spaces. This way the modules or functions belong to categories (culture, ecology, energy and mobility) and each has an impact on the other.
[SUPER BARRIO](http://superbarrio.iaac.net/)

*MAKING SENSE* is a platform that with an open-source technology, hardware and software (Smart Citizen kit), plus pilot learning creates a network that encourages and empowers citizens to play the leading role in designing, building and implementing technical and social innovations in their own houses.  
[MAKING SENSE](http://making-sense.eu/)

In the U.S. they have *BLOCK BY BLOCK*, that is centered in MineCraft videogame as a tool for visualization. This design platform is so easy to use that it has been transformed into a powerful form of communication. Using 3D models instead of architectural drawings improves the participants’ level of understanding and commitment.
[BLOCK BY BLOCK](https://www.blockbyblock.org/)

In the art scene we have the project *PLACE* as a collaborative project and social experiment hosted on the social networking site Reddit that started on April Fools' Day 2017. The experiment involved an online canvas of one million (1000x1000) pixel squares, which registered users could edit by changing the color of a single pixel from a 16-colour palette. The experiment was ended by Reddit administrators around 72 hours after its creation.
[PLACE](https://www.reddit.com/r/place/)

## What materials and components will be required?

I will require the users to download the API for free, to login and to share their personal data as photographies, videos, recorded audios and site locations.

## Where will they come from?

Most of the users will be neighbors of a particular district in Barcelona, where the public design will take place. Also any citizen can participate, but the purpose is to contact people that lives nearby or close to the empty slot and that will use that public space in a future time.

## How much will it cost?

The API will have a cost for programming, updating and for running with an important and capable **SERVER**, having enough space to store information of the users and to give real time outputs.
For the first task I will need a database management system designed to handle large amounts of data, as well as different servers for having different assignments.  
For the second task I will provide the platform with a Content Delivery Network system that would make it work faster.

As regards software engineering, the development of the frontend, will have to consider displaying the state of the board in real time, to facilitate the user’s interaction, by allowing them to upload images/audios/videos/location and to work on all platforms, including mobile apps.

*The answers to the questions above will allow you to create your BOM, or Bill Of Materials*
## What parts and systems will be made?

![]({{site.baseurl}}/NETWORK.jpg)

A **SERVER** to provide access to the *database*and to a *website*. This server will be the communication with the users, once they download the *APP* and throughout all the platform's performance. It will be the way to upload inputs from our mobile sensors (images, audios, videos, location as *COMPONENTS*) and to give feedback to the users on line, in a *textual notification* format. For every different action the users will have to communicate with the server: for example, if they want to participate actively, or if they want to only see the changes on the 3d model, rendering on real time, or if they want to have a private conversation with another user.
The server will be also connected to a *public web*, so that all the changes and people on line could be valued from the psychical site of the public space.
It would count with another server, independently, that would work with the *process of image and audio recognition*, as well. The idea is to have it separately to avoid collapsing the system.
Another step, for making the system work faster, will be using a *Content Delivery Network*. This system would be at a previous step to the server, to give a faster update of the 3d model to the user without running and reading the whole structure over.  

## What processes will be used?

THE DEVELOPMENT OF THE FRAMEWORK:
- **Identification**: The 1st phase involves identifying *matters of concern* that citizens care about, engaging themselves into a co-creative work for a public space in the neighborhood
- **Framing**: The 2nd phase involves identifying and explaining how the technology and data can be used to help tackling it and also taking care for any gap in resources or knowledge that need to be filled. This phase also is essential to manage expectations.
- **Deployment & Orchestration**: The 3th phase involves the deployment of the app to be tested, iterated and improved by the community. To test how the participants can collect data and how people interact with the different instructions on their natural environments. In this phase it may be necessary to organize events (i.e. data jams, hackathons or meet-ups) to enable social interactions between neighbors with different levels of knowledge and facilitators. The goal is the development of skills to ensure the correct use of the app and the participation of many citizens.
- **Outcome**: The last phase will be gathering and organizing all the data to ponder with the community and how the goals were or were not achieved. Once reached a consensus, this information will be delivered to the final architecture studio, Construction Company or any public entity that will be in charge of the construction phase.

## What tasks need to be completed?

I will need to start investigating and evaluating the existing models of Machine Learning to decide which will be the best and complete model to use for image and audio recognition. I could also design my own personal model, merging one existing model with another.
Once this selection is done, it would allow me to interpret the different inputs I will receive from the users and to understand how to use them into the design of the 3d model/public place.
This analysis would help me also to improve the platform, and to improve the components or instructions I will need to give both to the users and the server.
A back a forward from the software to the hardware, designing the product in a holistically manner.

## What questions need to be answered?

- Would this app will be accepted and used by the communities?
- Would the neighbors engage and understand how this new technology for gathering and using real data could be applied in a participatory design?
- Could this app manage and interpret the information that users will upload, giving them real time rendering and impact of their individual and collective decisions?
- Would this app help developers to improve public spaces for citizens?
- Could this app change the way we participate in our city, how we create and share useful data and how we invest our time in digital devices?
- Could this new methodology make architects and designers think of their role as a mere technical facilitator?
- Could this app change the existing and analogic methodology we use for co-creation in participatory design?

## What is the schedule?
![]({{site.baseurl}}/SCHEDULE.png)

## How will it be evaluated?

My evaluation will be from the community that I have chosen to work out the pilot: Can Batlló in the district of Sants [CAN BATLLÓ](https://www.canbatllo.org/). The neighbors of this community will be the ones to provide with the feedback to improve my digital interface with the real experience and the use of the app.
I will also get feedback for the IaaC faculties in the final exhibition, to help me reflect on better possible uses of this new methodology and this new form of co-creation for participatory design.
