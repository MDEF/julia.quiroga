---
title: 09. EMBEDDED PROGRAMMING
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---
## EMBEDDED PROGRAMING

This week we have learned about some components of our board.
The first and most important component is **ATtiny44**, the microcontroller. The number 44 refers to how much can be stored in it. Inside this microcontroller we can find different memories that store different things. We have the *SRAM memory* that stores the program and in addition the *EEPROM memory* for data storage. Also we have the *DDR (Data Director Register)* that specifies if it is an Input or an Output and the *PORTS* that defines which is the value of the pin.

![]({{site.baseurl}}/BLOCKDIAGRAM.jpg)

For this assignment we have to program a **ATtiny44** microcontroller on our Button + LED board, using the Arduino software.

First of all I’ve measured the current flow on my board with a MULTIMETER. I’ve checked that all traces and components were connected by following the GRD (ground) and VCC (power supply) paths.
Second, I have to check if the ARDUINO BOARD was working, by a basic code example as the *Blink* one (Files > Examples > 01.Basics > Blink)
Afterwards, I installed **ATtiny support** in Arduino 1.6.4, using the built-in *Boards Manager*. And I specified the kind of microcontroller we had: ATtiny44, with an external 20MHz clock. I have also selected the appropriate item from the Tools > Programmer menu *Arduino as ISP* as I was using an Arduino board as the programmer.

I’ve tried to upload the *Blink* code into my board by changing the *pinMode(LED_BUILTIN)** value to the one my physical led was connected. This was really easy to see, while following the traces on your board. In my case I had two leds: one connected to 1(VCC) and the other connected to 6(PA7). Here there is a graphic/map that shows the equivalences of the pins between **ATtiny44** and Arduino.

![]({{site.baseurl}}/EQUIVALENCES.png)

The code didn’t work out and kept on showing me the following error:

**Arduino: 1.6.11 (Windows 10), Board: "ATtiny24/44/84, ATtiny44, External 20 MHz"
avrdude: stk500_recv(): programmer is not responding**
