---
title: 05. ELECTRONIKS PRODUCTION
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---

This week was about the production of a PCB Board.
*A printed circuit board (PCB) mechanically supports and electrically connects electronic components or electrical components using conductive tracks, pads and other features etched from one or more sheet layers of copper laminated onto and/or between sheet layers of a non-conductive substrate.*

![]({{site.baseurl}}/1.jpg)

During this week we worked with one layer of copper (copper as a first layer and paper + glue as the second and non-conductive layer) and we first passed through the process of milling the traces and then through the welding process of the components.

![]({{site.baseurl}}/2.jpg)

I would like to make a distinction between the different ways of production in the board:
1. we can mill it with a CNC router
2. we can immerse it in a chemical bath that takes away the copper
3. we can laser-cut it
4. we can use a vinyl cutter

![]({{site.baseurl}}/3.png)
![]({{site.baseurl}}/4.jpg)

For this assignment we've worked with the first option (milling the board with a CNC router **SRM-20**)
The stages for using this machine are pretty specific and they are divided between the hardware and the software.   
In the CNC router we can trace and we can cut and, for each task we have different tools and different files to upload.

First, we have to prepare the board and place it on the bed of the CNC router. Here, it is important to add some tape (double tape without overlapping) so the board maintains its position during the graving process. This is very important for the remaining of the milling process.
Then, we should trace the tracks of our board. This means that we are going to use 1/64 inch mill for such purpose. The *VPANEL* for the **SMR-20** will allow us to use its commands to move and to direct the mill. We have to set up the origin point in x/y and z. For x and y, we can use the command on *VPANEL*, but for the z point we have to be careful enough and work out by hand for the last detail.

![]({{site.baseurl}}/5.png)

This origin set has to be established each time we change the mill. That's why if we use a mill 1/64 inch for tracing, with a specific x/y/z points, by the time we cut the board with another mill (1/32inch) we would have to set up them again.
Once this has been done and saved, we can open the *FAB MODULS* software to prepare the file. We import a .png file, a different one for each process: **TRACES** for the graving process and **INTERIOR** for the cutting process. We used an already design board, just to understand the production.

![]({{site.baseurl}}/6.png)
![]({{site.baseurl}}/7.png)

There are some parameters that we have to adjust in this software:
1. the type of machine we are using (Rolland Mill)
2. the process we are going through (TRACES 1/64 or OUTLINE 1/32)
3. the *dpi* has to be > 500
4. In the **OUTPUT**: *Name of the Machine*: SRM-20
                                          *Speed (mm/s)*: 4 (for traces) / 2 (for outline)
                                          *X0/Y0/Z0*: 0mm
                                          *ZJog*: 10mm
                                          *Z(home)*: < 10 (if is not predetermined)

5. At the end we can **CALCULATE** and **SAVE**  
This is the process we have to carry out each time for each function of the machine, either for the hardware -when we change the mill for cutting- or for the software, when we import the INTERIOR file.

Along each task, we have the opportunity to **PAUSE** the machine and to have a quick **VIEW** of what is tracing or cutting, and if everything is correct we can **RENEW** the process.

![]({{site.baseurl}}/11.jpg)

The last step before soldering is to sand the board in order to take out the protective layer of copper. This is very important so the tin sticks properly and fast to the board. This was the first thing I notice was happening to me, so I fix the problem sanding the board with a
metal mesh sponge.

The soldering process is something that you have to approach with patience and time. It is a minuscule scale and the tin dries instantly. We work with different components: a multiprocessor, a few capacitors, a few resistors, a few diodes, a crystal and a USB connector.  
It is interesting to encounter both sides of this production. The engineering side, if we can put it this way, where the machine's exactitude and precision is taken in account; and the craftsman side, where you are the tool to sold the components and then, locate them in the specific place of the board.

![]({{site.baseurl}}/8.jpg)
![]({{site.baseurl}}/9.jpg)


For this last stage, we use a plan that shows us where each component goes and how it is located and if you have to consider the orientation of the component when it is polarized (+/-), for example with the diodes. A previous introduction of basic electronics was very useful to start understanding how the current circulates through the board and how the components are organized for this commitment.
![]({{site.baseurl}}/10.png)

I had encountered a beginner's problem when I started to solder the most difficult component and the first one too: the ATtiny 44 multiprocessor. It had several contact points to solder and they were all very close to each other. My problem was to solder it and stick it to the board in the wrong orientation. This is something that you should take into account because, if not, all the circulation of the current will start to work with error.
![]({{site.baseurl}}/12.JPG)

There are two ways to unsolder a component of the board without removing the traces of copper. The first way is to melt the tin, using a copper braid. Also called a solder wick, this tool is a braid made from fine copper wires. You heat the copper on the tin you want to take off and the hot braid will absorb the tin, releasing its point of contact from the board.
![]({{site.baseurl}}/15.jpg)

In this case, it was very difficult to proceed with this technique, so I used a heat gun to heat the entire multiprocessor and all its contact points. I warmed it neither very closely nor for a long time; just long enough to remove the component and not to melt the traces of the board. As from this error I have paid more attention to the orientation of the components, trying not to make mistakes again.
![]({{site.baseurl}}/14.jpg)


At the end I have a ¨hero shot¨ of my final board. The next step would be connect it to my computer and start the recognition and programming.
![]({{site.baseurl}}/HERO SHOT.png)
