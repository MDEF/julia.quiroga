---
title: 02. MANAGEMENT OF THE PROJECT
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

Hi there my experience with Git Lab!
Finally I’ve got to understand the lOgic of this platform.

## GIT LAB  
**Git Lab** is a REPOSITORY where I can see and administrate my folders and my documents, as text in .pdf or as images. It helps me to also see all my movements and modifications in time and to also share with the group the process of coding.
For my particular case, I use it as a guide one of my companions’ instructions and codes. Just copy and paste, then modified the internal names for my project’s route.

## ATOM  
Also I decided to use **ATOM** as a platform for writing and updating. It is really friendly although there are some basics actions to take in account:
1. (ALWAYS) save/Ctrl S
2. Stage All
3. write down the name of this modification (so you can reach it later, in case you need to)
4. Commit to Master

![]({{site.baseurl}}/ATOM.png)

The last action will be **Pull**, so that these modifications can be reflected in the website.
These are the COMMAND LINE INSTRUCTIONS to upload the content to the repository in this interface.

I have to admit that the time of this last process is not that immediately. I suppose it depends on the traffic of users at the same time. Something that can help you is to **Fetch** everything.
Also you can have access to your website of-line by **Git Bash** for Windows, where you can request the link to track your changes.

![]({{site.baseurl}}/GIT BASH.png)   

## MARK DOWN  
Another particularity of the ATOM platform is that you can create *.md files*, MarkDown archives that can be written in MarkDown code, instead of HTML. It's a little easier to remember and to internalize when you start to document.
https://www.markdownguide.org/basic-syntax

-IS IMPORTANT to pay attention at the kind of document you are modifying (.md / .html), because that is what determines the kind of language you can use.


Once I have the basic template, it is possible to modify some things like the index or sub-index, the type/colour/size of the basic font, the images that are shown and also the short icon of the webpage (FAVICON).

Some issues to get down for my next update:
1. Is VERY IMPORTANT to be organize with the folders.
Each folder would be the basic route for you webpage to function as you designed it. Within any folder you can create subfolders, but is important to create a .md document, so that the platform can register a change.
Each document must have the following header:


![]({{site.baseurl}}/mdfile.png)

2. Once you create a folder, is important to create also a layout (.html document) with the same name. This document will have a code, that is the same code for any .html in the *Layout* folder:

![]({{site.baseurl}}/html.png)


Is important to fill the gaps with the same name of the folder, so it can follow the route of the .url

3. At the last stage you will modify the *config.yml* document, by adding the new custom collection (also in the list of *collections* and in the list of *defaults*) Filling the gaps with the same name of the folder created.

![]({{site.baseurl}}/config.png)

I MAY CONCLUDE to keep the same name for the folders (including the same blank spaces and capital letters), the documents and also the coding is VERY IMPORTANT, so that can the route for updating the webpage is direct.


AlsO you have another important documents to modify and customize your webpage.
In the **includes** folder you can modify the *header.html* to add more tabs in the principal INDEX, also the *footer.html* (adding your personal email or also your personal webpage) and at the end the *head.html* to change the FAVICON (short for favorite icon) of you site.

*header.html*  
![]({{site.baseurl}}/header.png)

*footer.html*  
![]({{site.baseurl}}/footer.png)

*head.html*  
![]({{site.baseurl}}/head.png)



In the **sass/utilities** folder you can modify the *base.scss* document to play around with the type/colour/size of the basic font. Is important to keep on the fonts that are authorized by the template and also to change the parameters that you have for the headings.

ABOUT the images in general,  you have to save each one in each folder related to the document that will include them. You can have several formats of images, .jpg/.jpeg/.png, but always remember to write the appropriate type of file.

<code>
"![]({{site.baseurl}}/________.jpg/.jpeg/.png)"
</code>


That would be all for this phase of MANAGEMENT OF THE PROJECT.
GOOD LUCK and enjOy!
