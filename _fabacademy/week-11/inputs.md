---
title: 11. INPUTS
period: 1-7 October 2018
date: 2018-10-08 12:00:00
term: 1
published: true
---
## INPUTS

This week, I've decided to work both of the assignments together.
I started thinking about my final project for the Master MDEF, and the idea of capacity sensors began to attract me. I’m not really sure if I’m going to use them, in the end, but from all the list of inputs, capacity sensors were the ones that most interested me.

I researched on many possibilities for working with this type of sensors and I got a pretty good idea from another student of  [Fab Lab New Cairo]( http://archive.fabacademy.org/fabacademy2017/fablabegypt/students/469/input%20devices.html), for her assignment **INPUTS DEVICES**


## TOUCH-SENSITIVE FUNCTIONALITY
Capacitive touch sensors fall into two general categories: the mutual-capacitance configuration and the *self-capacitance configuration*
The former, in which the sensing capacitor is composed of two terminals that function by emitting and receiving electrodes, is preferred for touch-sensitive displays. The latter, in which one terminal of the sensing capacitor is connected to ground (GND), is a straightforward approach that is suitable for a touch-sensitive button, slider, or wheel.
[ALL ABOUT CIRCUITS]( https://www.allaboutcircuits.com/technical-articles/introduction-to-capacitive-touch-sensing/) + [DESIGN WORLD ON LINE](https://www.designworldonline.com/capacitive-sensors-measure-low-forces/)

![]({{site.baseurl}}/CAPACITANCE.jpeg)

I decided to use this last example, a self-capacitance configuration, as an *input* for my PCB board, and worked with a LED LIGHT for the *output*

Let’s start form the beginning!

I've downloaded the schematic and board from its webpage and tried to understand the way it was designed.
I found the following list of components:
-	1 microprocessor ATtiny44
-	2 resistors 10K
-	1 resistor 499 with + 1 led light
-	1 header of 6pins connected to the programmer (I used a AVRISPmkII, which they use in FabAcademy)
-	1 header of 4pins to connect my jumpers to the handmade touch pads
-	1 header of 6pins that I had to modify with tweezers to solder it in a vertical position for connecting the PCBoard to my computer and run the Arduino code
-	1 capacitor of 1 uf, and
-	I added 1 ceramic resonator 20MHz as an external clock, because de original file didn’t have one

![]({{site.baseurl}}/SCH.png)
![]({{site.baseurl}}/BRD.jpg)

Then, I uploaded the .png file to FAB MODULES to check the traces and outlines before milling. It was important as well to check the size of the final board.

![]({{site.baseurl}}/FABMT.png)
![]({{site.baseurl}}/FABMO.png)

The milling and soldering phase went really well. The only issue was that I worked with a machine I had never used before: **ROLLAND MDX20**, that was set too slowly for the milling process. Also the *ControlPanel* of this machine, works different from the other. So it was a new experience to handle.
This control panel, sets X and Y from the software, but the *Zhome* it’s set manually from the machine.

![]({{site.baseurl}}/ROLANDMDX20.jpg)

![]({{site.baseurl}}/CONTROLPANEL.jpg)
![]({{site.baseurl}}/CONTROLPANEL LF.jpg)

![]({{site.baseurl}}/ROLANDMDX20z.jpg)

Therefore, I set up the *Zhome* a little bit deeper than it was necessary, and milled a little bit deeper, as required. Nevertheless, the final board was OK and the traces were perfect.

![]({{site.baseurl}}/MILLING.jpg)

The soldering stage was not a problem at all.
The final PCBoard with the components already soldered looks as in this picture:

![]({{site.baseurl}}/HEROO.jpg)

I verified with the multi-meter if all the important connections were OK:
**GND** connections between the headers with the microcontroller **ATtiny44** as well as **VCC power** were all beeping.

![]({{site.baseurl}}/MULTIMETER.png)

Before starting with *Arduino* and programming my board, I decided to finish the hand-made part and to build my *handmade touch pads* with some cardboard, stick copper and jumpers. On one side, the copper connected or in contact with the jumpers and in the other side the cardboard.

![]({{site.baseurl}}/TOUCHPADS.jpg)

For **PROGRAMMING** with *Arduino* I follow the instructions of [Embedded Programming Fab Academy 2019]( http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html) + [high low tech]( http://highlowtech.org/?p=1695) to program an ATtiny44 microcontroller on my board. The path was very easy to follow. Also, to make *Arduino* software recognize my board.

![]({{site.baseurl}}/ARDUINOTOOLS.png)

Then I connected everything as it was explained, paying attention on the right assembly between the headers: checking to connect the **GND** from the header in my board to the **GND** in the **AVRISPmkII**. This step is essential for the correct functioning and recognition of the board! I decided to use **AVRISPmkII** as a PROGRAMMER, because it shows me with a red or green light if everything is alright for the next steps.

![]({{site.baseurl}}/PROGRAMMING1.png)

Afterwards, I started coding in *Arduino*, with the **BLINK** example from 01.Basics and I changed on the **pinMode(LED_BUILTIN, OUTPUT)** the LED number as well as in **digitalWrite(LED_BUILTIN, HIGH)**
I found out which was the number that corresponded to my LED in the PCBoard, connected to the microprocessor and corresponding to an Analog Input, in the *ATtiny44 Data Sheet*. In this case was number 7, so I replaced with this number the **INPUTS** of the code.

![]({{site.baseurl}}/DATASHEET.png)
![]({{site.baseurl}}/ARDUINONOT.png)
#### I first got worried with this message, but I’ve checked on Google and it was OK, it was just a notification.

I run the code with the new parameters and an **ERROR** notice appeared:

**avrdude: stk500v2_command(): command failed
avrdude: stk500v2_program_enable(): bad AVRISPmkII connection status: Unknown status 0x00
avrdude: initialization failed, rc=-1
         Double check connections and try again, or use -F to override
         this check.**

![]({{site.baseurl}}/ARDUINOERROR.png)
