---
title: FINAL INTERVENTION
period: 1-7 October 2018
date: 2018-10-06 12:00:00
published: true
---
## PARTICIPATORY DESIGN

*I would like to emphasize the role of the architect as a mere technical facilitator*, as a curator and connector that serves the community and its public needs. *(Lucien Kroll - Participation Movement / 1970)*

## CONTEXT

The context of my intervention is in the community. I´m interested in social collaboration, as well as participation, interdisciplinary approaches and collective networks. The purpose is to make possible and visible everyone´s participation, applying a horizontal starting point and maintaining that same objective through the entire development of the project. The final delivery is to add more information, to make each person a part of the project, to embrace the multiplicity and to connect them all.

My project particularly is based and founded in a self-managed community in the district of La Bordeta | Sants called *BlocOnze*. Nowadays they manage part of an industrial complex jointly with the Ajuntament de Barcelona, known as Can Batlló.
Can Batlló is a 14-hectare former textile manufacturing complex which dates back to **1878**. The complex was the backbone around which the urban structure of La Bordeta’s neighborhood was organized. In its most prosperous moment it was developed as an authentic manufacturer's town, home to more than 2000 workers. The business closed as a textile factory at the beginning of the **1960s**, leaving space for the occupation of the warehouses and workshops by various small businesses, a transition that generated a particular industrial ecosystem.
In **1976** *Barcelona’s General Metropolitan Plan* responded to the demands of residents for public facilities and designated the land of Can Batlló as an area for public facilities and green spaces.
Up until the **1990s**, the urban transformation of Barcelona was not focused on the Sants-Montjuïc District.
In **2000**, however, a significant transformation was planned through Gran Via, a major avenue that crosses Sants, connecting Barcelona with nearby l’Hospitalet. The objective was to develop a new economic district around the Gran Via, with the expansion of the Fira de Barcelona exhibition centre and the construction of the so-called ‘Ciutat de la Justícia’ (an area housing the principal bodies of the Catalan justice and legal system). This urban development raised new expectations on the part of the owners of Can Batlló, who hoped to transform their plots into housing developments.
In **2002** more than 200 small industrial businesses were operating in the Can Batlló complex.
In **2006** the City Council passed an urban redevelopment proposal which would maintain the manufacturing complex, provide for a green area of 4.7 hectares, and approve a significant development of housing stock, including 300 apartments dedicated to social housing.
This plan did not satisfy the expectations of the multinational enterprise (Gaudir) that owned the land and which instead requested approval for the construction of larger, high-rise apartments. The operation was subsequently suspended following the bursting of the housing bubble, leaving Can Batlló’s lands and buildings as they had been, with some workshops still functioning inside the manufacturing complex.
In **2009**, several neighbors and social organizations launched a platform called Recuperem Can Batlló: Can Batlló és per el barri [Reclaiming Can Batlló: Can Batlló is for the neighborhood]. Various types of social communities and organizations, such as Comissió de Veïns de La Bordeta or Centre Social de Sants, joined this platform. They established a specific date, June 1st 2011, by which they demanded the public administration to make a decision, and that, in the absence of such decision by that date, they would occupy the complex to reclaim the space.
June 1st **2011** was precisely the date on which a new conservative Mayor took office. The change of the City Council took place within a social climate of citizen mobilization and effervescence led by the Indignados movement.
In this context, the new Mayor decided - having reached an agreement with the owner - to assign the management of part of the Can Batlló complex to the residents’ platform. Specifically, Recuperem Can Batlló was granted the management of a 1500 m2 building known as *‘BlocOnze’*.
A year later, the platform had renovated and transformed the building, creating a bar, an auditorium, a library and other spaces. Some of the resources that enabled the development of these spaces came from the City Council, but the autonomy of the process' management was always maintained by the organization.
The platform *BlocOnze* is organized around an Assembly that carries out monthly meetings and distributes its work throughout various commissions. It is, then, a social initiative where things are being achieved through a network of commissions and autonomous projects, each of which is horizontally managed according to a rationale of democratic cooperation.
[CAN BATLLÓ, una realitat](https://vimeo.com/24545336)

Nowadays *BlocOnze* manage part of this industrial complex jointly with the Ajuntament de Barcelona, meeting to the necessities and requirements of the neighborhood. It has 18 common spaces self-managed that work during the week and weekends, supplied by *BlocOnze´s* own neighbors:

![]({{site.baseurl}}/CANBATLLO.jpg)
![]({{site.baseurl}}/MAPCB.png)

#### 1.Espai de trobada / bar  2.Auditori  3.Biblioteca Popular Josep Pons  4.Nau 69  5.Taller d'infraestructures  6.Fusteria col·lectiva  7.Impremta col·lectiva  8.Arts Can Batlló  9.Centre de Documentació  10.Taller de cervesa  11.Espai arts escèniques  12.La Nau Espai Familiar  13.Mobilitat Can Batlló  14.Zona de gossos  15.Horts comunitaris  17.Zona esportiva  18.La Garrofera de Sants - Cooperativa ecològica autogestionada  19.La capella - espai d'exposicions


The initiatives of DIY are approached from the physical and from the organization of the cooperative. They have meetings, assemblies, festivals and a digital platform that connects each neighbor with the community and the decisions made [CAN BATLLÓ](https://www.canbatllo.org/)

As regards participatory design, this actual platform has a non structural methodology where TIME has not been taken into consideration. I had the opportunity to get to know the actual procedure for new projects with [Cooperativa LACOL](http://www.lacol.coop/) and one of the unknown variables was time and the genuine inclusion of all the neighbors.
This Cooperativa is in charge of many of the rehabilitation approaches at the complex, but their main project was Cooperativa d’habitatge La Borda (2017/2018), a house living project for the people of the neighborhood.

## AREA OF INTEREST / DESCRIPTION OF THE INTERVENTION

This project's goal is to deliver a digital, new and friendly platform for participatory designs, providing the users with a structured platform that will keep them informed in real time where and how their comments and desires are a useful instrument for designing public spaces.
The project's purpose also is to include all neighbors and active users in order to merge their reflections and ideas, working with and encouraging a horizontal platform, accessible to everyone and with a real time output.

This platform's target is to show how it is possible from an *instantaneous picture* to access other kind of information and data, which underlies this already common habit of taking pictures, generating a self-awareness and a responsibility for our own actions.
I believe that digital language is the one that will be the main way of communicating in the near future. This new era, when we are stuck almost all the time to a digital device, will change our communications and thus, our relationships. I'd rather take this scenario as one that I can benefit from, instead of fighting against the predictable future. Digital language provides us with many opportunities to have in mind and to take advantage of, such as expressing yourself through digital images and interacting and connecting with multiple users at the same time, having a transparent flow of information, receiving outputs and results within a short period of time, being an inclusive and accessible network to everyone.

As regards digital language, it´s important to observe that most of the time we are in a state of *connected*. Obviously, this is not something purely related to the digital communication, but instead this is related to the gamification of some platforms, which appeal to our minds. Sensing the empowerment and engagement provided by any game, in some way it maintains our anxiety and our attention. That´s why this new project, besides being based on digital communication, it will have gamification parameters. The idea is to devote this digital time that we have already taken, in something useful for the society. Participation does not have to be something boring. On the other hand, gaming methodology delivers a playful environment for everyone, leaving apart the technical issues for some of the tasks and allowing the participation at any age. With this initiative we attempt to teach older and younger people to understand digital devices and carry out digital literacy.

*¨…It was a dynamic environment that could easily be adapted and changed, allowing inhabitants to explore their creativity through play and interaction.¨ (Constant Nieuwenhuys - New Babylon / 1956)*

The purpose of the new application will be to provide useful DATA from the pictures uploaded by the neighbors who login. I will start using the *Instagram platform* to engage neighbors and to work with the images they already have in their devices. This information will be very valuable to understand which are the main characteristics of the district involved and what are the desires of the population we are taking into account for this project.
The goal is to inform and to connect people, to show the real parameters; and, in that way, to encourage everyone's design. This social and transparent approach is fundamental for the intervention.
The structure of the application will be displayed through different *stages of analysis*, where each stage has its score or, in other words, its urban diagnosis. The final result will be an *evaluating matrix* that will show all the results collected and will give an approximation of which aspect we have to take care of in order to reach an ideal environmental quality.
To contextualize the designs in a real scenario, with actual priorities and guidelines, is also essential for the project. The users as well as the neighbors need to know and to consider all the evaluation indicators, so to play in a real and grounded world. This interface will present the issues to be solved, based on the results obtained, and it will show the neighbors' opinions from a real and instantaneous approach.
Finally, this platform will be nourished by the users' needs and propositions. In this way the app will be presented as a *flexible and adaptable algorithm*, enabling a space for interventions and redesigns for each particular district and each individual project. Thus, and due to this feature, the project is still under *construction*.

## STATE OF THE ART

## [SUPERBARRIO](http://superbarrio.iaac.net/)
It’s a videogame to boost participatory design.
This platform is based on *gamification strategies* to empower and engage citizens in the design of public spaces.
It works on a neighborhood scale, willing to approach the necessities and desires of the users and neighbors.
*It is used as a tool for architects, urban planners and public entities to acknowledge what are people’s designs and needs*. Also it instructs citizens on sustainability and inclusiveness, *showing the impact of individual and collective decisions*.  

This platform allows citizens to visualize and navigate in the 3D model of the neighborhood.
The methodology is to interact with the space, allocating already designed modules or assigning a function to empty spaces. This way the modules or functions belong to categories (culture, ecology, energy and mobility) and each has an impact on the other.
*There is a system of scores and metrics* that shows this impact on the overall neighborhood and *textual notifications* encourage the player to keep the balance between the different values. At the end, the users are rewarded with medals as long as they are capable of maintaining this balance between the different values of the categories.

The data visualization of the results it’s a map and can be filtered according to different parameters (gender, age, job) and it’s a strong tool for decision makers. This data visualization is only accessible to authorized users.
It is a flexible tool that can be used in different scenarios, providing the same rules, score logic and data collection.

![]({{site.baseurl}}/SUPER.jpg)


## [BLOKbyBLOCK](https://www.blockbyblock.org/)
It’s a Foundation that using MineCraft as a template, approaches the participation of the community in the urban design process.
It is centered in this *popular videogame as a tool for visualization, actively engaging the neighbors* who many times don’t have voice in public projects. *The design platform is so easy to use that it has been transformed into a powerful form of communication*.
Using 3D models instead of architectural drawings improved the participants’ level of understanding and commitment.
*The whole process is structured in several stages*:
1. Model
Selects the site for intervention, and creates a 3D model of the existing site using images, plans, Google maps, and other inputs.
2. Mobilize
Clusters people living and working near the site who are interested in improving their urban environment.
3. Organize& Teach
Organizes community workshops and trains participants in the fundamentals of using Minecraft. It identifies an agent to lead training and provide support.
Basic aspects of the public space and general design considerations are also introduced to contextualize the project.
4. Present & Prioritize
The teams present their models and advocate their ideas to professionals that include urban planners, architects and local policy makers.

This platform models also informs cost estimates, budget allocations, and professional design work.

The projects selected for this methodology are projects that become integrated in their surrounding environments, *sometimes placed on the specific interventions and sometimes more on the process and the people involved*.

 ![]({{site.baseurl}}/BLOCK.jpg)


## [DECIDIM](https://decidim.org/)
This digital open-source platform helps citizens, organizations and public institutions to self-organize democratically at every scale.
It can be used in a public or private organization, with hundreds or thousands of potential participants, configuring SPACES for participation (initiatives/processes/assemblies/consultations), through multiple available COMPONENTS (comments/proposals/voting/face-to-face meetings/surveys).

This platform provides an organization with a complete toolkit for easily designing and deploying a democratic system and adapting it to the user’s needs.

How does it work?
The SPACES for participation are the frameworks that define how participation will be carried out. They are the channels through which citizens can process requests or coordinate proposals and make decisions.
The participatory COMPONENTS are participatory mechanisms that allow operations and interactions between the users and the participatory spaces.
The way in which both interact is that the participatory SPACES, such as Initiatives, Assemblies, Processes and Consultations, have COMPONENTS at their disposal that work together as participatory mechanisms.
The more notable components include In-person Meetings, Surveys, Proposals, Debates, Results and Comments.

*Participants can carry on different types of actions within the platform*:
to navigate and search for information / to create contents / to vote, support or sign their preferences for a specific consultation, proposal or initiative / to comment on any object of the platform / to follow other participants and receive notifications / to sign up for a meeting.
*Also they are grouped into three different categories*:
- Visitors that have access to the entire platform’ content without having to sign up or provide any information.
- Registered by choosing a username, password, accepting the user agreement, and providing an email account.
*Registered participants can also make their account official (meaning their username is accompanied by a special symbol indicating they really are who they claim to be on their profile)*
- Verified; in order to fall under this category, they must first be verified as members of an organization, citizens of the municipality, or constituents of the decision-making group.

Participants can not only navigate the content of the platform through the top menu and move down from a space to its different components; *they can also get information through notifications and talk to other participants by internal messaging*.

*The home page of the platform is fully customizable*: it has different types of banners, buttons and it can also display statistics and interactive visualizations, like maps with the activities and the upcoming meetings.

  ![]({{site.baseurl}}/DECI.jpg)


## [MAKINGSENSE](http://making-sense.eu/)
Making Sense explores how open-source software, open-source hardware, digital maker practices and open design can be effectively used by local communities to appropriate their own technological sensing tools, to make sense of their environments and to address pressing environmental problems on air, water, soil and sound pollution.

Making Sense is a platform that with an open-source technology, hardware and software (Smart Citizen kit), plus pilot learning creates a network that encourages and empowers citizens to play the leading role in designing, building and implementing technical and social innovations in their own houses.  
*The project starts with the real needs of citizens and communities*, who want to understand their own environment’s actual problems.

The project uses open-source technologies, such as Arduino, to enable ordinary citizens to gather information on their environment and make it available to the public on the Smart Citizen platform around the globe.

The Smart Citizen kit is a tool for data capture and analysis. It has sensors to collect urban data accurately and *it’s easy to use*. The purpose of this tool for citizens’ actions is to monitor the environment and to raise awareness in order to find solutions for issues that matter to the community, fostering citizens through participatory data collection, analysis and action.
The three types of information that are included in the Toolkit are:
- tools and resources that explain step by step how you should set up your kit,
- case studies, guidelines, and resources, and
- testimonials to make it more tangible and fun

*The Making Sense project consists in a framework with the following components*:
- identifying a community and their needs,
- generating awareness,
- on-boarding Toolkit for the community,
- introducing the data sense-making process,
- organizing workshops that connect citizens to Making Sense as well as each other,
- teaching the Making Sense Toolkit, its impact and outputs

The pilots are driven by human needs, not by the technology that is available. Making Sense refers to what makes sense to the citizen participants, as *data that is understandable*, relevant, useful, timely and actionable.
*It facilitates the transition of citizens from being “passive downloaders of data to active uploaders of action.”*

 ![]({{site.baseurl}}/MAKINGSENSE.png)


## [ARTURO – 300.000kms](http://arturo.300000kms.net/#1)
Arturo is the name of *an automated learning algorithm designed to determine what are the urban conditions that make cities more habitable* and thus, helping technicians and developers.

With all the data gathered from the participants, an open database is created, which *allows to convert citizens' subjective perception into reusable knowledge*.
The experiment is about measuring the city's habitability, in order to understand if it is possible to make the idea of habitability objective, to quantify and to measure it. Also to understand which urban parameters are the most influential for this quantification.
Instead of trying to build a single possible description of what a habitable city is like, *the project wants to build a collective description through the participation of the largest number of citizens*.

To achieve this goal, *the project asks citizens to evaluate a series of photographs from different streets of the city*. Through the input of the valuations, a list with the best and worst rated streets is obtained. Once this set of images has been organized (representing 10% of the total), the algorithm will be able analyze what are the common characteristics among those who share similar voting standards. Each photograph has been previously classified, associating up to 50 urbanistic parameters such as the density built, the land uses, the geometry of the buildings, their age, their constructive quality and other more complex values.
The experiment developed on this occasion provides, for the first time, an interpretation of the results from the urban values that are associated with each image - assuming that the visual experience of space is the consequence of the parameters that define each urban mesh.

 ![]({{site.baseurl}}/ARTURO1.png)
 ![]({{site.baseurl}}/ARTUROO.png)



 ![]({{site.baseurl}}/STATEART.png)
 ![]({{site.baseurl}}/TIMELINEF.png)



## INTERVENTION’ S DESIGN ACTION

[My first intervention was for the second term review:
I decided to run a prototype application for a mobile device, materializing my ideas and projections on how the methodology of participatory design would be in the future; shifting from the already known generative tools in the analogical way towards a digital interface that allows accessibility and celerity into the participation action.
I couldn't test this app with the actual community, but I've shown it to faculties and designers related.]

For the third term, my intervention will be divided in different STAGES.
These phases will overlap each other and work simultaneously. One phase will be carried out outdoors, in the field of action, and the other will be indoors, working with the new tool.

## 1st STAGE
One of the phases, maybe the more relevant and challenging, will be working within the community. This approach has already started and it is the key for analyzing, understanding and envisioning my project. This community is Can Batlló, situated in the district of Sants.
It has several activities and workshops that take place during the week and weekends as well.
Certainly, the Bar is the place for gathering and giving information about other activities.
During the week it works in the morning and in the night shifts and, on Saturdays, all day long. It has access to the Auditori and to the Library on the main floor, and to the Rocòdrom on the second floor.
Lots of young people pass by, have a beer and wonder which activity is taking place in the rest of the areas. There is another kind of public that is enjoying and gathering outside: having some sport activity, walking around with their kids and, meeting with other parents or neighbors, biking, or just passing through since this is a public street, to shorten their way.
This public has a peak schedule around the afternoon hours, at this time kids get out of school or people stop working. Also, there is a Family Space in one of the areas that makes it possible the gathering and meeting. The third type of public is the one that comes for specific workshops on the agenda.

To contact this community has not been easy.
The first thing I've done was going to the place, in person, because the email contact with dissenyespai or secretaria didn't work.
I found out by visiting the place that there are some key steps that I need to follow:
1. the open workshop for creating and printing posters, and
2. the next General Assembly
These two events would help me both to approach the community and to come closer to the people.

The first step will be an anonymous approach: the idea of creating provocative posters to attract the attention of the people and to engage them in the project it's related to activism campaign. Printing these posters with the people of the community will give me a partial feedback. I will attach some stickers to the posters and also use an Instagram platform to gather people's photographs.
The idea is to ask the neighbors to take pictures of **what they like the most at Can Batlló and to upload these pictures with #construimcanbattlo**. This will help me to collect information from the neighbors of this community. I will not use filters since I need raw data: everyday emotions, thoughts, desires, and dreams about this place. Just the parameters, which belong to the common and local people.

![]({{site.baseurl}}/IMPRENTACOLECTIVA.jpg)
![]({{site.baseurl}}/POSTERYSTICKER.png)


1. I went to the open workshop for printing posters and I encountered a scenario and approach much too different than the one I had thought in the first place.
Instead of taking any personal work that you bring and teach you how to do it, using their machines; the instructors at this workshop are OPEN to welcome people to help them in any task they are carrying out at that given moment. These tasks could be cleaning, re-accommodating things and materials or printing, at best. They told me to talk directly with the Activitats people of Can Batlló to make sure to start from the beginning. This should be my first step: showing them my project and moving forward with the posters and broadcasting.

I also realized that for my first contact with the leaders of this community, I would need a landing page (attached to the Instagram account) to explain a little bit more about the project. Trying to get people's attention and collaboration without explaining the entire project is something that would not work at all.
For this purpose, I would work on the template of WordPress (WordPress.com) and by achieving a personal domain.

![]({{site.baseurl}}/CONSTRUIM1.png)
![]({{site.baseurl}}/CONSTRUIM2.png)
![]({{site.baseurl}}/CONSTRUIM3.png)

## TAKE A PICTURE AND PARTICIPATE
Our platform proposes the users to be creative, sharing their thoughts, wishes and dreams through digital photographs.
A trained algorithm will be responsible for discovering the data behind the images and grouping the results related to the citizen's opinion.

WHY Construïm?

- **ACCESIBILITY:**
It is ACCESSIBLE and free from any digital device connected to Wi-Fi, either Android or iOS. You can participate by logging in and uploading the photos in a very simple way.
- **TIME:**
The photographs can be uploaded at any time of the day; the users are the ones who manage their involvement and participation. The results will be updated daily and will be displayed in REAL TIME.
- **INCLUSION:**
The algorithm will be trained moment by moment, as users share their images. All the data of this new collective methodology will be included and grouped by analogy.
- **VISUALIZATION:**
The data obtained will be reflected on an interactive map of the site, differentiated by colors for the indicators of urban design: *NATURE AND SPORTS / HUMAN INTERACTION / HERITAGE / UTILITIES / CIRCULATION*
- **PARTICIPATION:**
The final result will be a tool for the current planning of public spaces; providing citizens information in an original, clear and direct way.
- **ADAPTATION:**
This methodology is adaptable to any scale and space of citizen participation. Its license is open source, so that it can be used by different communities or public entities.

My purpose is to get in touch with the organization, first of all, to communicate my project and second to make them "partners in crime”.
This last step is a huge one, but I have already started by visiting the place, by trying to understand the internal dynamics, and how this self-community is structured. The next opportunity will be the Assembly.


2. I attended the General Assembly and stayed for the whole session. It was very interesting to see how this Assembly was organized and which were the main topics that the Council was considering to address. First of all, there was one representative (minimum) of each area of Can Batlló. The Council brought up some main and general topics, some of them informative and others that required the participation and vote of the people present at the Assembly. These general topics were related to the participation of Can Batlló in other city events; about the robbery in one of the areas; and, a future interview for a film, that one of the members of the Secretary was going to attend.
Then, the representatives of some specific areas talked about particular topics and many of them asked for more information.  I could witness an active participation or even voting.
In this second phase there were interesting discussions that came about:
-	The voting system; the way nowadays the Can Batlló Assembly was reorganized to be open for more participation; and, the final results of any new task/action/entrepreneurship. Finally, the representatives also considered if the system was working or not.
-	Another interesting discussion was about which would be the admission criteria for new projects.  
This was a very interesting experience because it allowed me to understand and to know how the dynamics and the bureaucracy were managed. This self-managed community works under the structure of any Assembly, where there is no verticality and all the decisions are taken by all its members.
Anyway, there are some paths to follow for any new proposal. The main area for filtering it is the Secretaria. Once you pass this area, you can pass the proposal to the rest of the committee.
You find this procedure within the whole structure of Can Batlló, but you have another structure in the Assembly: an area proposes an idea, the Assembly is convened, people of the other areas can make any questions or comments, while a coordinator organizes the order of the speakers, and finally the participants vote.

 ![]({{site.baseurl}}/ASAMBLEA.jpg)
 ![]({{site.baseurl}}/ASSEMBLYY.jpg)


3. I moved forward with the posters and broadcasting on my own.
The results have been good so far! I've distributed 10 posters in many of the areas that I consider relevant for the community. At the entrance, of course, at the main bar, also some in front of the different workshops, some others in spots for the people passing by or exercising. For this distribution, I tried to think on the different people of the community.

 ![]({{site.baseurl}}/POSTER1.jpg)
 ![]({{site.baseurl}}/POSTERSS.jpg)

 After this, many people started posting on the Instagram platform, requested to follow or posted specific questions about the project and, one of this people even uploaded pictures!
Of course it is a matter of time and effort to generate more advertising: on the one hand by hanging more posters and, on the other, by getting the contribution of some of the areas of Can Batlló. And also, to get the neighbors involved in the project.


## 2nd STAGE
The second phase is working with the interface, because Instagram will not be the final tool to gather the data of the community.
I'm planning to design a new interface for participatory design to connect and to share the data that the neighbors upload. I want to shift from analog methodologies to a digital one, to be more accessible, to be more open and to let people express their thoughts, desires and dreams in the way they already know how: using their mobile device and taking an instant and digital picture.

For this purpose, the digital platform has to be easy and friendly to use. The way to engage users is not to complicate them, this is the final goal. That's why Instagram, Snapchat and WhatsApp are the most popular among the other interfaces.

So, I will be able to study how the participants work, why they prefer to use images or short stories or even recorded audios; to analyze how and why they pass from one stage to the other, why they add filters, why they are different from each other and finally, what makes people use these platforms and why they prefer one or another. This is the context for my new participatory interface.

![]({{site.baseurl}}/INSTAGRAM2.png)
![]({{site.baseurl}}/INSTAGRAM1.png)



## FINAL REFLECTION

This has been a journey towards a new phase in my life, the beginning of an opportunity.  Certainly, there were issues I'd rather take into account and solve during the first term, but as an architect I know and I have already got through this feeling of ‘forced landing’.
For instance, the design stage couldn’t take for ever. To take your ideas out of the box, connect them and try to select just the good and feasible ones, to sketch a reasonable projection and then present it to a new and real community; for sure, it was something difficult to achieve. But in my opinion, I have arrived at a good outcome.
I had something clear from the beginning: the area of interest and the community I would like to work with. The most challenging task for me was trying to get to ground all the inputs and ideas that my area of interest was embracing. Because when we talk about **PARTICIPATORY DESING**, the spectrum is quite broad. There are many angles from where you can reach this area, fix the different aspects, and approach the various agents involved. *So what was the aspect, within participatory design, I was looking at?*
The design practice allows me to have a perspective and a projection of what I would like to attain in the future, and from this exercise I decided to carry on and to take advantage of the situation we are facing nowadays. A new paradigm where collectiveness, horizontal structures and bottom-up methodologies are gaining ground; where my desires as an architect in favor of interdisciplinary teams are taking more and more space. Opening the box and finding out that my individual search, coming from my personal background, has something to do with the actual context was something that helped me channeling my project.
So now, we are talking about the **METHODOLOGY** in PARTICIPATORY DESING.
All the inputs and, certainly, the *Research Trip to China – Shenzhen*, made me realize that methodologies can go a step beyond the mere romantic approach. The advances made by the European Union and China as regards the digital world astonished me and made me take a huge step concerning my intervention.
In the beginning I was quite idealistic and I had an old-school point of view on how methodologies could or might change in participatory design in order to really make every citizen participate.
The language gap between us, Western audiences, and China - Shenzhen was really overwhelming and my beliefs about the way to communicate ideas changed immediately after landing. At first I found myself judging, but soon after I realized that China was the future as regards technology and that was something we should accept and learn how to work with.
It’s impossible to think of changing the way things are going and growing. Also, to have a look of what is happening in one of the most powerful countries in the world was something I would always be thankful for. Travelling and stepping out of your own bubble -most of the time your comfort zone- helps you to expand your vision and to position yourself a step nearer to the future, providing the knowledge, the perspective and the tools to face it.
So, by the second term, my intervention was pretty close to my final project. But then again I had to deal with a bunch of ideas, inputs and a lot of design processing to put my project in paper. Now that I can reflect about this process, this would be the perfect moment to work out a little bit more rigorous within my intervention. To immerse myself in the technological field of my intervention, to involve myself with the community and to get feedback from a more local context, in relation to my interface. But, at that moment, I was given some other assignments and time was limited.
So after a second term where I couldn't advance my project as much as I would have liked it, the final submission began.
I was able to keep on improving my intervention and defining the way I could use and take advantage of the technology that we have at present and in the future.
Personally, this was something really difficult to achieve: thinking and designing for the emergent future. Certainly, having in mind the near future is something all designers are trained for. Nevertheless, to speculate and imagine a future 30 years from now or even more is something that is not easy.
First, my imagination wandered too far, like around a science-fiction world, but soon I realized that it wasn’t the purpose at all. In this sense, the workshops I took and the faculties really helped me to control my aspirations for a faraway future and, also to work with the trends and the imaginable scenarios. It was a matter of handling a little piece of information from both sides and to project what I would like and need to happen in order to contextualize my intervention and to allow it to evolve.
This particular vision of the Master was really useful and motivating for thinking my intervention as a feasible and possible action, working with real technological advances on a real scenario. From this point, the project gained in strength and began to materialize. Now, it was the time to face the technical details and engage the community.
For this second task, my position and my self-esteem certainly were already determined. But when I *faced the community*, I found out that its inner structure was pretty close. This first shock almost made me lose the little faith I already had, but I kept on trying to make contacts, to make some noise, and to relate with the neighbors. The first relationships I made were not the key ones, but they were the ones that made me trust again and helped me to express my project out loud and outside of my comfort zone. Also these contacts helped me to reach more people and to keep on getting involved with the community.
Going deeper and deeper into the Community and being aware of the journey I went through from my first shock until today when I feel more secure about myself, enabled me to ‘hang out’ my intervention. Also, keeping always a positive attitude, improved the neighbors' participation and the communication worked quite well.
Indeed, it is a matter of time and compromise, but for now, the platform is working and people is providing me with inputs. Right now, I know this is the only thing I could ask for since my project is not even finished nor launched. But it is very satisfying to receive feedback from a community that is already developed, but whose members are willing to help. I’m really glad to have experimented the transition from the first rough shock to the actual state because the feeling of doing something that matters to others and that is happening in the right place and time, is pleasing and encouraging.   
In relation with the technical details, I've faced the fact that an objective and horizontal design doesn’t exist for real scenarios. My idea of using Machine Learning for interpreting the images from the citizens/users, and learning their inner thoughts and desires without any kind of manipulation of such data, is something naïve.  Certainly, it is better than having designers, developers or businessmen trying to fit their ideas within the community and thus, influencing the final results. Yet, someone has to train the algorithm for the first time and show how to consider that data.
From this knowledge, which I faced in the last stage of my intervention, my enthusiasm decreased rapidly. Because somehow, my whole project's goal was to reject any kind of data manipulation and to work the interdisciplinary methodology among the citizen’s raw and ordinary information, without misunderstandings or external influences.
Nevertheless, going deeper into the technical matters of my intervention, made me realize that someone had to take the first step, the first code towards this ‘objective’ machine; someone had to push the button, and that was where the romantic idea faced reality.
Anyway, losing enthusiasm didn't make me leave my project behind. In fact, it made me redefine my position and confront the reality. Again, I decided to stop judging and tried to work for a better, fair and emergent future.
So the question is if I am happy enough about the results at this stage … I could answer that I am, but I have this feeling that *I’m just starting out*.

![]({{site.baseurl}}/MOCKUP.png)
![]({{site.baseurl}}/MOCKUP.jpg)
![]({{site.baseurl}}/EVALUATINGMATRIX.png)

[LAUNCH VIDEO](https://youtu.be/2BppGhBDw-0)
