---
title: MY SECOND TERM JOURNEY
period: 1-7 October 2018
date: 2018-10-06 12:00:00
published: true
---
![]({{site.baseurl}}/ATLAS.png)

![]({{site.baseurl}}/TUTORIAL-1.png)
![]({{site.baseurl}}/TUTORIAL-2.png)
![]({{site.baseurl}}/TUTORIAL-3.png)
![]({{site.baseurl}}/TUTORIAL-4.png)
![]({{site.baseurl}}/TUTORIAL-5.png)
![]({{site.baseurl}}/TUTORIAL-6.png)
![]({{site.baseurl}}/TUTORIAL-7.png)



# REFLECTIONS
## PARTICIPATORY DESIGN in PUBLIC SPACES

This second term I presented myself in a different way: I submitted a prototype for a project that took me to the area of interest. Now, I know where I want to work, what I’m looking for and I have a quite a good idea of what it could be my goal.
While working on my prototype for participatory design, the idea of imagining the user, its necessities and preferences, made me feel right on track.
I already had some inputs from different perspectives of participatory approaches, like designers from SUPERBARRIO videogame (Master in City & Technology – Iaac) and from Cooperativa LACOL, a cooperative of architects that works in Can Batlló with social housing. Both references were pretty dissimilar, because of the way they handle this kind of methodologies. On the one hand we have a platform, completely digital and with an inclination for gamification and, on the other hand we have the old school methods with meetings and face-to-face debates. Both had good resolutions and bad resolutions, but it was interesting to get to know these two perspectives and try to take the best of each.
This was something I submitted for consideration during my final presentation, how to conceive a new platform that can be better and accessible to everyone,  without losing time, but also without losing any personal detail.
The reactions from the faculties were varied. But we can cluster them into two important groups:
-	The idea of carrying on planning a digital platform for participatory design, with a tendency for the user's gamification, was something that captured the attention on several of the faculties. Of course there were certain elements to upgrade, but the idea of **abstract the subject**, to simulate (instead of gamification), to connect the neighbors, to **merge experiences** between different users, to cluster in groups, to play with **agents** (good and bad agents within the community), to have different stages as a **trigger**, to work at real time, **to make the difference to each one that is registering in the app**… ultimately, a platform receiving useful data from the users through a ludic experience.
Certainly, this is the beginning and it’s important to take into account what could happen when the users are thousands: How the app would consider everyone? What would be the final delivery? What would the designer do with all the inputs?
Yet, I have an answer for this last question: what if the conclusive output of all this experience hasn’t to be necessarily a final project? Instead, this will be all data drawn from  and about the citizens. Information that can be used to plan the strategy for the final project, to educate the neighbors, to understand what their real need is.

-	The other group of faculties was in the line of working with real people. I have already chosen the place for the intervention, and the community behind this space. This kind of feedback made me understand and also realize what is the real meaning of PARTICIPATORY DESIGN: when I’m designing by myself, with the idea that I have of future users and the possible inputs. I came to understand it was a contradictory route the one I was heading for. To communicate and design for the real meaning of participatory / collaborative methodologies, it was necessary to take inputs from real citizens and neighbors. It was important to stop designing only from my ideas, but instead put these preconceptions on the test.

So here I was, with two lines of advice, which, at first, I thought they couldn’t coexist. Nevertheless, I came to realize that one feed the other, in the way one strategy gives me the inputs to improve the other.
As feedback, another recommendation that several faculties gave me was to take a look at the “state of the art” that already exist around this topic. Not only the two I have mentioned before, but many others worldwide and, also to analyze them and to get more inputs for my final project.



At this point I have several works to head on:
## In a GLOBAL CONTEXT
1.	**STATE OF THE ART**: A deep analysis of the “state of the art”, related to participatory design in digital platforms (Decidim, SuperBarrio, Block-Hood, Making Sense, Block by Block)
2.	**AJUNTAMENT & PLATFORM CAN BATLLÓ**: Get information about the history, the context and the background of this particular site. Get to know the present regulations as regards urbanism and public spaces. Try to answer to myself some key questions about this self-managed organization and how it is arranged today (Ajuntament de Barcelona / Can Batlló organization)
## DESIGN OF THE APPROACH
3.	**METHODOLOGIES**: Look at many examples and methodologies to approach a community, assemble workshops, create a reflection space (in group or individual)
## APPROACH
4.	**STRATEGY**:
a)	Contact the people involved in the Platform of Can Batlló, trying to reach the Disseny de l’espai or Secretaria General to propose an idea/organize an act
b)	Contact the people of Impremta Col·lectiva Can Batlló to design and create posters to hang outside the site, like an anonymous approach to the people, from posters and motivational phrases
Design a strategy to approach the community of Can Batlló and get the inputs of real users and neighbors. Understand what is the real need, the MATTER OF CONCERN.
5.	**REDESIGN**: With all the physical inputs, whatever and how many they are, improve and redesign the app to feed it with the local context.
